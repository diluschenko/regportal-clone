<?php

namespace Drupal\regportal_flag\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\regportal_flag\RegportalFlagService;
use Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ModalForm class.
 */
class UserFavoritesClearForm extends FormBase {

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * The flag service.
   *
   * @var \Drupal\regportal_flag\RegportalFlagService
   */
  protected $regportalFlagService;


  /**
   * Class constructor.
   *
   * @param AccountInterface $account
   *   The current account.
   * @param \Drupal\regportal_flag\RegportalFlagService $regportal_flag_service
   *   The regportal flag service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function __construct(AccountInterface $account, RegportalFlagService $regportal_flag_service) {
    $this->account = $account;
    $this->regportalFlagService = $regportal_flag_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('regportal_flag')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'regportal_user_favorites_clear_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {

    $form['#prefix'] = '<div id="favorites-form-wrapper">';
    $form['#suffix'] = '</div>';
    
    $form['info'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#value' => $this->t('Вы действительно хотите удалить все отбранные фирмы?'),
      '#attributes' => [
        'class' => [
          'form-message',
        ],
      ],
    ];

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Удалить'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    drupal_set_message($this->t('Отобранные фирмы были удалены'));
    $response->addCommand(new RedirectCommand(Url::fromRoute('regportal_user.orders', ['user' => $this->account->id()])->toString()));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->regportalFlagService->unflagAllByUserAndFlagId($this->account, 'favorites');
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['config.regportal_order_form'];
  }

}