<?php
/**
 * @file
 * Contains \Drupal\regportal_order\RegportalOrder.
 */

namespace Drupal\regportal_order;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\user\Entity\User;
use Drupal\Core\Datetime\DrupalDateTime;

class RegportalOrder {

  /**
   * @const
   */
  const ENTITY_MACHINE_NAME = 'node';

  /**
   * @const
   */
  const BUNDLE_MACHINE_NAME = 'order';

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var SqlEntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->storage = $entityTypeManager->getStorage(self::ENTITY_MACHINE_NAME);
  }

  /**
   * Set discount for user.
   *
   * @param \Drupal\user\Entity\User $user
   *    The user.
   * @param int $current_discount
   *    Current order discount.
   *
   * @return int
   *    User discount.
   */
  public function setUserDiscount(User $user, $order_discount = 0) {
    $discount = 0;

    $date = new DrupalDateTime('-7 days midnight');

    $query = $this->storage->getQuery()
      ->condition('type', self::BUNDLE_MACHINE_NAME)
      ->condition('uid', $user->id())
      ->condition('field_order_status', ORDER_STATUS_CLOSED)
      ->condition('field_order_status_date', strtotime($date->format(DATETIME_DATETIME_STORAGE_FORMAT)), '>=');
    $nids = $query->execute();

    if ($nids) {
      $orders = $this->storage->loadMultiple($nids);
      foreach ($orders as $order) {
        $discount += $order->field_order_discount->value;
      }
    }

    $user->field_user_discount = $discount + $order_discount;
    $user->save();

    return $discount;
  }

}
