<?php

namespace Drupal\regportal_order\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\regportal_flag\RegportalFlagService;
use Drupal\user\Entity\User;
USE Drupal\Core\Url;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\Entity\Node;
use Drupal\Core\Mail\MailManagerInterface;

/**
 * ModalForm class.
 */
class OrderForm extends FormBase {

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * The flag service.
   *
   * @var \Drupal\regportal_flag\RegportalFlagService
   */
  protected $regportalFlagService;

  /**
   * The mail manager
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * Class constructor.
   *
   * @param AccountInterface $account
   *   The current account.
   * @param \Drupal\regportal_flag\RegportalFlagService $regportal_flag_service
   *   The regportal flag service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The mail manager.
   */
  public function __construct(AccountInterface $account, RegportalFlagService $regportal_flag_service, MailManagerInterface $mail_manager) {
    $this->account = $account;
    $this->regportalFlagService = $regportal_flag_service;
    $this->mailManager = $mail_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('current_user'),
      $container->get('regportal_flag'),
      $container->get('plugin.manager.mail')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'regportal_order_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $options = NULL) {
    $user = User::load($this->account->id());

    $form['#prefix'] = '<div id="order-form-wrapper">';
    $form['#suffix'] = '</div>';

    // The status messages that will contain any form errors.
    $form['status_messages'] = [
      '#type' => 'status_messages',
      '#weight' => -10,
    ];

    $form['info'] = [
      '#markup' => $this->t('Пожалуйста, проверьте контактные данные по которым с вами можно будет связаться.'),
    ];

    $form['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ваше имя'),
      '#title_display' => 'invisible',
      '#default_value' => $user->get('field_user_visible_name')->value,
      '#placeholder' => $this->t('Ваше имя'),
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('E-mail'),
      '#title_display' => 'invisible',
      '#default_value' => $user->get('mail')->value,
      '#placeholder' => $this->t('E-mail'),
      '#required' => TRUE,
    ];

    $phone = $user->get('field_user_phone')->isEmpty() ? '' : $user->get('field_user_phone')->value;
    $form['phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Телефон'),
      '#title_display' => 'invisible',
      '#default_value' => $phone,
      '#placeholder' => $this->t('Телефон'),
      '#required' => TRUE,
    ];

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['send'] = [
      '#type' => 'submit',
      '#value' => $this->t('Отправить заказ на исполнение'),
      '#attributes' => [
        'class' => [
          'use-ajax',
        ],
      ],
      '#ajax' => [
        'callback' => [$this, 'submitModalFormAjax'],
        'event' => 'click',
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * AJAX callback handler that displays any errors or a success message.
   */
  public function submitModalFormAjax(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#order-form-wrapper', $form));
    }
    else {
      drupal_set_message($this->t('Ваш заказ принят и отправлен на обработку, менеджер свяжется с Вами с течение 15 минут.'));
      $response->addCommand(new RedirectCommand(Url::fromRoute('regportal_user.orders', ['user' => $this->account->id()])->toString()));
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $storage = $form_state->getStorage();
    $flags = $this->regportalFlagService->getUsersFlags($this->account, 'node', 'company');
    $bag = $flags['bag'];

    $flaggings = $this->regportalFlagService->getUserFlaggings($bag, $this->account);
    if (!count($flaggings)) {
      $form_state->setError($form, $this->t('Ваша корзина пуста.'));
    }
    foreach ($flaggings as $flagging) {
      $company = $flagging->flagged_entity->referencedEntities()[0];
      if ($company->field_company_status->value != COMPANY_STATUS_ON_SALE) {
        $form_state->setError($form, $this->t(
          'Компания «@label» забронирована другим пользователем. Для продолжения офрмления заказа, пожалуйста удалите ее из корзины.',
          ['@label' => $company->label()]
        ));
      }
    }

    $storage['flaggings'] = $flaggings;
    $storage['flags'] = $flags;
    $form_state->setStorage($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $user = User::load($this->account->id());
    $current_discount = !$user->field_user_discount->isEmpty() ? $user->field_user_discount->value : 0;
    $storage = $form_state->getStorage();
    $flags = $storage['flags'];
    $flaggings = $storage['flaggings'];
    $flag_bag = $flags['bag'];

    $ids = [];
    $total_price = 0;
    $order_discount = 0;
    $companies = [];
    foreach ($flaggings as $flagging) {
      $company = $flagging->flagged_entity->referencedEntities()[0];

      // Clear the bag.
      $this->regportalFlagService->unflag($flag_bag, $company, $this->account);

      $ids[] = ['target_id' => $company->id()];
      $total_price += $company->field_company_price->value - $current_discount;
      $order_discount += ($company->field_company_price->value - $company->field_company_seller_price->value) * ORDER_DISCOUNT;
      $company->field_company_status = COMPANY_STATUS_BOOKED;
      $company->field_company_buyer->target_id = $user->id();
      $company->save();

      // Prepare data for user mail.
      $companies[] = [
        'title' => $company->label(),
        'ogrn' => $company->field_company_reg_number->value,
        'price' => $company->field_company_price->value,
      ];
    }

    $order_email = $form_state->getValue('email');
    $order_username = $form_state->getValue('username');
    $order_phone = $form_state->getValue('phone');

    // Create an order.
    $order = Node::create([
      'type' => 'order',
      'title' => 'Закаказ от ' . $this->account->getAccountName() . ' ' . date('d.m.Y H:i'),
      'uid' => $this->account->id(),
      'field_order_companies' => $ids,
      'field_order_status' => ORDER_STATUS_IN_PROGRESS,
      'field_order_total_price' => $total_price,
      'field_order_discount' => $order_discount,
      'field_order_current_discount' => $current_discount,
      'field_order_user_name' => $order_username,
      'field_order_user_mail' => $order_email,
      'field_order_user_phone' => $order_phone,
    ]);

    $order->save();

    // Generate url order form form.
    $url = Url::fromRoute('entity.node.edit_form', ['node' => $order->id()], ['absolute' => TRUE]);

    // Send email to manager.
    $module = 'regportal_order';
    $key = 'order_in_progress_to_manager';
    $to = 's.zhukov@regportal.su';
    // Send email to user.
    $message = [
      '#theme' => 'regportal-order-mail-to-manager',
      '#link' => $url->toString(),
      '#order_name' => $order->label(),
      '#companies' => $companies,
      '#discount' => $current_discount,
      '#phone' => $order_phone,
      '#email' => $order_email,
    ];

    $params['message'] = render($message);

    $send = true;
    $langcode = 'ru';
    $this->mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);

    // Send email to user.
    $message = [
      '#theme' => 'regportal-order-mail',
      '#full_name' => $order_username,
      '#order_name' => $order->label(),
      '#companies' => $companies,
      '#discount' => $current_discount,
      '#phone' => $order_phone,
      '#email' => $order_email,
    ];

    $key = 'order_user_mail';
    $to = $user->mail->value;
    $params['message'] = render($message);
    $result = $this->mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
      drupal_set_message(t('Извиняемся, но произошла ошибка при отсылке вашего заказа менеджеру. Пожалуйста позвоните по телефону, чтобы Ваш заказ своевременно обслужили.'), 'error');
    }
  }

  /**
   * Gets the configuration names that will be editable.
   *
   * @return array
   *   An array of configuration object names that are editable if called in
   *   conjunction with the trait's config() method.
   */
  protected function getEditableConfigNames() {
    return ['config.regportal_order_form'];
  }

}
