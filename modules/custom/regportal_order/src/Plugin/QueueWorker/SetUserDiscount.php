<?php
/**
 * @file
 * Contains \Drupal\regportal_order\Plugin\QueueWorker\SetUserDiscount.
 */

namespace Drupal\regportal_order\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Set user discount.
 *
 * @QueueWorker(
 *   id = "set_user_discount",
 *   title = @Translation("Learning task worker: set user discount"),
 *   cron = {"time" = 10}
 * )
 */
class SetUserDiscount extends QueueWorkerBase {
  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    $regportalOrder = \Drupal::service('regportal_order.helper');
    $regportalOrder->setUserDiscount($data['user']);
  }
}
