<?php
/**
 * @file
 * Contains \Drupal\regportal_import\Form\CompanyUploadForm.
 */

namespace Drupal\regportal_company\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\regportal_company\RegportalCompany;

/**
 * Update status for companies.
 */
class UpdateStatusForm extends FormBase {

  /**
   * @var RegportalCompany
   */
  protected $regportalCompany;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    RegportalCompany $regportalCompany
  ) {
    $this->regportalCompany = $regportalCompany;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('regportal_company.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'company_update_status_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = $this->regportalCompany->getStatusOptions();
    $form['status'] = [
      '#type' => 'select',
      '#title' => $this->t('Выберите статус'),
      '#options' => $options,
      '#required' => TRUE,
    ];

    $form['ogrn'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Список ОГРН'),
      '#description' => $this->t('По одному в строку.'),
      '#rows' => 15,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Изменить статус'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $status = $form_state->getValue('status');
    $values = preg_split('/\r\n|[\r\n]/', $form_state->getValue('ogrn'));

    $count = 0;
    foreach ($values as $value) {
      $ogrn = $this->regportalCompany->parseOgrn($value);
      if ($ogrn['status'] == 'error') {
        drupal_set_message($ogrn['str'], 'error');
        continue;
      }

      $nid = $this->regportalCompany->getCompanyIdByOgrn($ogrn['number']);

      if (!$nid) {
        drupal_set_message(t('Компания с ОГРН: @number отсутствует в базе.', ['@number' => $ogrn['number']]), 'error');
        continue;
      }

      $this->regportalCompany->updateCompanyStatus($nid, $status);
      $count++;
    }

    drupal_set_message(t('@count компании обновлено.', ['@count' => $count]));
  }

}
