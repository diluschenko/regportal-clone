<?php
/**
 * @file
 * Contains \Drupal\regportal_company\RegportalCompany.
 */

namespace Drupal\regportal_company;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;

class RegportalCompany {

  /**
   * @const
   */
  const ENTITY_MACHINE_NAME = 'node';

  /**
   * @const
   */
  const BUNDLE_MACHINE_NAME = 'company';

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var SqlEntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->storage = $entityTypeManager->getStorage(self::ENTITY_MACHINE_NAME);
  }

  /**
   * Parse OGRN.
   *
   * @param string $str
   *
   * @return array
   *   Array with status and string.
   */
  public function parseOgrn($str) {
    $match = preg_match('/[^\d]*(\d{13})[^\d]+|[^\d]*(\d{13})$|[^\d]*(\d{15})[^\d]+|[^\d]*(\d{15})$/', $str, $output);

    if ($match) {
      $arr = [
        'status' => 'success',
        'number' => end($output)
      ];
    }
    else {
      $arr = [
        'status' => 'error',
        'str' => t(
          'Поле ОГРН должно содержать 11 или 13 цифр в строке @str',
          ['@str' => $str]
        ),
      ];
    }

    return $arr;
  }

  /**
   * Check company it the DB.
   *
   * @param string $ogrn
   *   Register number.
   *
   * @return int
   *  NID.
   */
  public function getCompanyIdByOgrn($ogrn) {
    $query = $this->storage->getQuery()
      ->condition('type', 'company')
      ->condition('field_company_reg_number', $ogrn);
    $nids = $query->execute();

    return count($nids) ? reset($nids) : 0;
  }


  /**
   * Update company status.
   *
   * @param int $nid
   *   Node id.
   */
  public function updateCompanyStatus($nid, $status) {
    try {
      $node = $this->storage->load($nid);

      if ($node->field_company_status->value != COMPANY_STATUS_SOLD_OUT) {
        $node->field_company_status = $status;
      }

      $node->save();
    }
    catch (Exception $e) {
      \Drupal::logger('regportal_company')->error($e->getMessage());
    }
  }

  /**
   * Update company.
   *
   * @param int $nid
   *   Node id.
   */
  public function updateCompany($nid, $values) {
    try {
      $node = $this->storage->load($nid);
      foreach ($values as $key => $value) {
        $node->{$key} = $value;
      }
      $node->save();
    }
    catch (Exception $e) {
      \Drupal::logger('regportal_company')->error($e->getMessage());
    }
  }

  /**
   * Get options for status.
   *
   * @return array
   *   Array of statuses.
   */
  public function getStatusOptions() {
    $statuses = [
      1 => 'в продаже',
      2 => 'забронирована',
      3 => 'продана',
      4 => 'продажа приостановлена',
    ];

    return $statuses;
  }
}
