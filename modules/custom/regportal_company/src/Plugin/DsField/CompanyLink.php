<?php

namespace Drupal\regportal_company\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Plugin that displays management links for the Availability node.
 *
 * @DsField(
 *   id = "ajax_link",
 *   title = @Translation("Ajax link"),
 *   entity_type = "node",
 *   provider = "regportal_company",
 *   ui_limit = {"company|*"}
 * )
 */
class CompanyLink extends DsFieldBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a NewsletterEditLinks object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance..
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings) {
    return ['No configuration available'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (isset($config['entity'])) {
      $entity = $config['entity'];
      $link = Link::fromTextAndUrl('Подробнее', Url::fromUri('internal:/' . 'node/' . $entity->id()))->toRenderable();
      $link['#attributes'] = [
        'class' => ['use-ajax'],
        'data-dialog-options' => '{"title": null, "dialogClass": "ui-dialog-blue ui-dialog-buy-company", "buttons": [{"label": ""}]}',
        'data-dialog-type' => 'modal',
        'onclick' => 'yaCounter35057690.reachGoal("more_info"); return true;'
      ];

      $build = [
        '#type' => 'markup',
        '#markup' => render($link),
      ];

      return $build;
    }

    return FALSE;
  }

}
