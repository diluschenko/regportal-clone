<?php

namespace Drupal\regportal_company\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Plugin that displays order phone.
 *
 * @DsField(
 *   id = "company_update",
 *   title = @Translation("Comapny Update"),
 *   entity_type = "node",
 *   provider = "regportal_company",
 *   ui_limit = {"company|updated"}
 * )
 */
class CompanyUpdate extends DsFieldBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a NewsletterEditLinks object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance..
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings) {
    return ['No configuration available'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (isset($config['entity']) && !$config['entity']->field_company_status->isEmpty()) {
      $entity = $config['entity'];
      $status_values = [
        1 => 'выставлена в продажу',
        2 => 'забронирована',
        3 => 'продана',
      ];
      $title = $entity->label();
      $status = $entity->field_company_status->value;

      if ((int)$status === 1) {
        $link = Link::fromTextAndUrl($title, Url::fromUri('internal:/' . 'node/' . $entity->id()))->toRenderable();
        $link['#attributes'] = [
          'class' => ['use-ajax'],
          'data-dialog-options' => '{"title": null, "dialogClass": "ui-dialog-blue ui-dialog-buy-company", "buttons": [{"label": ""}]}',
          'data-dialog-type' => 'modal',
          'onclick' => 'yaCounter35057690.reachGoal("more_info"); return true;'
        ];
        $title = render($link);
      }

      $build = [
        '#type' => 'markup',
        '#markup' => $title . ' ' . $status_values[$status],
      ];

      return $build;
    }

    return FALSE;
  }

}
