<?php
/**
 * @file
 * Contains \Drupal\regportal_user\RegportalUserHelper.
 */

namespace Drupal\regportal_user;

use Drupal\user\Entity\User;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Database\Connection;
use Drupal\node\Entity\Node;

class RegportalUserHelper {

  /**
   * Active database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * {@inheritdoc}
   */
  public function __construct(Connection $database) {
    $this->database = $database;
  }

  /**
 * Add new entity.
 *
 * @param \Drupal\user\Entity\User $user
 *    The user.
 *
 * @return string
 *    User display role.
 */
  public function getDisplayRole(User $user) {
    $roles = $user->getRoles();

    $role_name = t('Покупатель');
    if (in_array('chief', $roles)) {
      $role_name = t('Руководитель');
    }
    elseif (in_array('manager', $roles)) {
      $role_name = t('Менеджер');
    }
    elseif ($user->id() == 1) {
      $role_name = t('Админ');
    }

    return $role_name;
  }

  /**
   * Get user picture.
   *
   * @param \Drupal\user\Entity\User $user
   *    The user.
   * @param string $style_name
   *    The user.
   *
   * @return string
   *    Path to picture.
   */
  public function getUserPictureUrl(User $user, $style_name) {
    if (!$user->user_picture->isEmpty()) {
      $path = $user->user_picture->entity->getFileUri();
    }


    return isset($path) ? ImageStyle::load($style_name)->buildUrl($path) : NULL;
  }

  /**
   * Get user picture.
   *
   * @param \Drupal\user\Entity\User $user
   *    The user.
   *
   * @return int
   *    Counts of favorites
   */
  public function getCountFavorites(User $user) {
    $query = $this->database->select('flagging')
      ->condition('flag_id', 'favorites')
      ->condition('uid', $user->id());

    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Get user bag sum.
   *
   * @param \Drupal\user\Entity\User $user
   *    The user.
   *
   * @return array
   *    The one array's element where key is count, value is sum.
   */
  public function getUserBagSum(User $user) {
    $query = $this->database->select('flagging', 'f')
      ->fields('f', ['entity_id'])
      ->condition('flag_id', 'bag')
      ->condition('uid', $user->id());

    $results = $query->execute()->fetchAll();

    $total_sum = 0;
    $discount = $user->field_user_discount->isEmpty() ? 0 : $user->field_user_discount->value;
    foreach ($results as $result) {
      $company = Node::load($result->entity_id);

      $total_sum += $company->field_company_price->value - $discount;
    }

    $sum[count($results)] = $total_sum;
    return $sum;
  }



}
