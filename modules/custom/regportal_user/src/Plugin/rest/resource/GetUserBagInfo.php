<?php
namespace Drupal\regportal_user\Plugin\rest\resource;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;
use Drupal\regportal_user\RegportalUserHelper;
use Drupal\user\Entity\User;

/**
 * Provides a resource to get view modes by entity and bundle.
 *
 * @RestResource(
 *   id = "regportal_user_get_user_bags_info",
 *   label = @Translation("Get user bag's info"),
 *   uri_paths = {
 *     "canonical" = "/rest/get-user-bag-info"
 *   }
 * )
 */
class GetUserBagInfo extends ResourceBase {
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The flag service.
   *
   * @var \Drupal\regportal_user\RegportalUserHelper
   */
  protected $regportalUser;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\regportal_user\RegportalUserHelper $regportal_user
   *   A current user instance.
   */
  public function __construct(
      array $configuration,
      $plugin_id,
      $plugin_definition,
      array $serializer_formats,
      LoggerInterface $logger,
      AccountProxyInterface $current_user,
      RegportalUserHelper $regportal_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->regportalUser = $regportal_user;
  }
  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('example_rest'),
      $container->get('current_user'),
      $container->get('regportal_user.regportal_user_helper')
    );
  }
  /**
   * Responds to GET requests.
   *
   * Returns a user's bag info.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function get() {
    if ($this->currentUser->isAnonymous()) {
      throw new AccessDeniedHttpException();
    }

    $counts = $this->regportalUser->getUserBagSum(User::load($this->currentUser->id()));

    $data = [
      'counts' => key($counts),
      'sum' => current($counts),
    ];
    $response = new JsonResponse();
    $response->setData($data);
    return $response;
  }
}