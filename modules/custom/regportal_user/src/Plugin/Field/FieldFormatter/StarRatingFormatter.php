<?php

namespace Drupal\regportal_user\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'Starrating' formatter.
 *
 * @FieldFormatter(
 *   id = "starrating",
 *   module = "regportal_user",
 *   label = @Translation("Star rating"),
 *   field_types = {
 *     "starrating"
 *   }
 * )
 */
class StarRatingFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'fill_blank' => 0,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $field_settings = $this->getFieldSettings();
    $max = $field_settings['max_value'];
    $min = 1;
    $fill_blank = $this->getSetting('fill_blank');
    foreach ($items as $delta => $item) {
      $rate = $item->value;
      $elements[$delta] = [
        '#theme' => 'starrating_formatter',
        '#rate' => $rate,
        '#min' => $min,
        '#max' => $max,
        '#fill_blank' => $fill_blank,
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = [];
    $element['fill_blank'] = [
      '#type' => 'checkbox',
      '#title' => t('Fill with blank icons'),
      '#default_value' => $this->getSetting('fill_blank'),
    ];


    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    $field_settings = $this->getFieldSettings();
    $max = $field_settings['max_value'];
    $min = 1;
    $fill_blank = $this->getSetting('fill_blank');
    $elements = [
      '#theme' => 'starrating_formatter',
      '#min' => $min,
      '#max' => $max,
      '#fill_blank' => $fill_blank,
    ];

    $summary[] = $elements;

    return $summary;
  }

}
