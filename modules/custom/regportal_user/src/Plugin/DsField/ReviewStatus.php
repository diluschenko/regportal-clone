<?php

namespace Drupal\regportal_user\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin that displays management links for the Availability node.
 *
 * @DsField(
 *   id = "review_status",
 *   title = @Translation("Review status"),
 *   entity_type = "comment",
 *   provider = "regportal_user",
 *   ui_limit = {"otzyvy_o_prodavce|*"}
 * )
 */
class ReviewStatus extends DsFieldBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The entity query manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityQuery;

  /**
   * Constructs a NewsletterEditLinks object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance..
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings) {
    return ['No configuration available'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (isset($config['entity'])) {
      $entity = $config['entity'];

      $status = 'positive';
      $value = '+';
      if (!$entity->field_comment_rating->isEmpty() && $entity->field_comment_rating->value < 4) {
        $status = 'negative';
        $value = '–';
      }

      $status = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $value,
        '#attributes' => [
          'class' => ['user-review-status', ' user-review-status-' . $status],
        ],
      ];

      return [
        '#markup' => render($status),
      ];
    }

    return FALSE;
  }

}
