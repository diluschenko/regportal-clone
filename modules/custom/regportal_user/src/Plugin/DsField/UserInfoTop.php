<?php

namespace Drupal\regportal_user\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin that displays user review link.
 *
 * @DsField(
 *   id = "user_info_top",
 *   title = @Translation("User banner title"),
 *   entity_type = "user",
 *   provider = "regportal_user",
 *   ui_limit = {"user|business_card"}
 * )
 */
class UserInfoTop extends DsFieldBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs a NewsletterEditLinks object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance..
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings) {
    return ['No configuration available'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (isset($config['entity'])) {
      $entity = $config['entity'];

      $title = 'О регистраторе';
      if (!$entity->field_user_visible_name->isEmpty()) {
        $title = $entity->field_user_visible_name->value;
      }

      $h1 = [
        '#type' => 'html_tag',
        '#tag' => 'h1',
        '#attributes' => [
          'class' => ['topline', 'text-align-center'],
        ],
        '#value' => $title,
      ];

      $p = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#attributes' => [
          'class' => ['text-large', 'text-align-center'],
        ],
        '#value' => t('На этой странице вы найдете всю информацию <br> о регистраторе'),
      ];

      $p2 = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#attributes' => [
          'class' => ['text-align-center'],
        ],
        '#value' => '<a class="button follow-link" data-follow="field--name-user-title" href="#">' . t('Посмотреть') . '</a>',
      ];

      $build = [
        '#type' => 'markup',
        '#markup' => render($h1) . render($p) . render($p2),
      ];

      return $build;
    }

    return FALSE;
  }

}
