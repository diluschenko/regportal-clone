<?php

namespace Drupal\regportal_user\Plugin\DsField;

use Drupal\ds\Plugin\DsField\DsFieldBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Plugin that displays management links for the Availability node.
 *
 * @DsField(
 *   id = "user_raiting",
 *   title = @Translation("User Rating"),
 *   entity_type = "user",
 *   provider = "regportal_user",
 *   ui_limit = {"user|*"}
 * )
 */
class UserRaiting extends DsFieldBase implements ContainerFactoryPluginInterface {

  /**
   * The entity manager object.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * The entity query manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityQuery;

  /**
   * Constructs a NewsletterEditLinks object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance..
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary($settings) {
    return ['No configuration available'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (isset($config['entity'])) {
      $build = [];
      $entity = $config['entity'];

      $this->entityQuery = \Drupal::service('entity.query');
      $count = $this->entityQuery->get('comment')
        ->condition('comment_type', 'otzyvy_o_prodavce', '=')
        ->condition('status', 1, '=')
        ->condition('entity_id', $entity->id(), '=')
        ->count()->execute();

      if ($count) {
        $count = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $count . ' ' . t('  оценок'),
          '#attributes' => [
            'class' => ['user-rate-counts'],
          ],
        ];

        $query = \Drupal::database()->select('comment__field_comment_rating', 'cr');
        $query->addExpression('AVG(field_comment_rating_value)', 'avg');
        $query->join('comment_field_data', 'cd', 'cd.cid = cr.entity_id');
        $query->condition('cd.comment_type', 'otzyvy_o_prodavce', '=');
        $query->condition('cd.status', 1, '=');
        $query->condition('cd.entity_id', $entity->id(), '=');
        $result = $query->execute()->fetchAssoc();

        $stars = [
          '#theme' => 'starrating_formatter',
          '#rate' => round($result['avg']),
          '#min' => 1,
          '#max' => 5,
          '#type' => 'starrating',
        ];

        $build = [
          '#markup' =>  '<a href="/business-card/' . $entity->id() . '/#edit-group-reviews" class="follow-link-stars follow-link" data-follow="field--name-dynamic-block-fielduser-reviews">' . render($stars) . '</a>' . render($count),
        ];
      }


      return $build;
    }

    return FALSE;
  }

}
