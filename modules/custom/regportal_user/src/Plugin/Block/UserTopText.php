<?php

namespace Drupal\regportal_user\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UserTopText.
 *
 * @Block(
 *   id = "regportal_user_top_text",
 *   admin_label = @Translation("User Top Text"),
 *   category = @Translation("Custom Blocks"),
 * )
 */
class UserTopText extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'text' => $this->t('О регистраторе'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#format' => 'full_html',
      '#default_value' => $this->configuration['text']['value'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['text']
      = $form_state->getValue('text');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return ['#markup' => $this->configuration['text']['value']];
  }

}
