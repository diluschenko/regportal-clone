<?php

namespace Drupal\regportal_user\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\user\Entity\User;

/**
 * Class UserTopText.
 *
 * @Block(
 *   id = "regportal_user_role_name",
 *   admin_label = @Translation("User Role Name"),
 *   category = @Translation("Custom Blocks"),
 * )
 */
class UserRoleName extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $user = User::load(\Drupal::currentUser()->id());
    $role_name = \Drupal::service('regportal_user.regportal_user_helper')->getDisplayRole($user);

    $name = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#value' => $user->getAccountName(),
    ];

    return [
      '#markup' => $role_name . ' ' . render($name),
    ];
  }

}
