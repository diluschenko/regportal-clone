<?php
namespace Drupal\regportal_user\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides route responses for the Example module.
 */
class RegportalUserController extends ControllerBase {

  /**
   * @param int $uid
   *    The user id.
   *
   * @return array
   *   A renderable array.
   */
  public function userCommentForm($uid) {
    $values = [
      'entity_type' => 'user',
      'entity_id' => $uid,
      'field_name' => 'field_user_review',
      'comment_type' => 'otzyvy_o_prodavce',
    ];
    $comment = \Drupal::entityTypeManager()->getStorage('comment')->create($values);
    $form = \Drupal::service('entity.form_builder')->getForm($comment, 'default');

    $element = array(
      '#markup' => render($form),
    );

    return $element;
  }

  /**
   * Get title of page for business card.
   *
   * @param \Drupal\user\Entity\User $user
   *    The user.
   *
   * @return string
   *   The title of page.
   */
  public function userBusinessCardTitle($user) {
    $title = $user->getUsername();
    if (!$user->field_user_visible_name->isEmpty()) {
      $title = $user->field_user_visible_name->value;
    }

    return $title;
  }

  /**
   * Checks access for a regportal_user.orders route.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   */
  public function accessOwnOrders(AccountInterface $account, $user) {
    if ($account->id() == $user) {
      return AccessResult::allowed();
    }

    return AccessResult::forbidden();
  }

}
