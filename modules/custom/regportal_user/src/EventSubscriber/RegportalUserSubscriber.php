<?php

namespace Drupal\regportal_user\EventSubscriber;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RegportalUserSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public function userBusinessCard(GetResponseEvent $event) {
    $route_name = $event->getRequest()->get('_route');

    if ('regportal_user.business_card' !== $route_name) {
      return;
    }

    $user = $event->getRequest()->get('user');
    $roles = $user->getRoles();

    if (!in_array('registrar', $roles)) {
      throw new NotFoundHttpException();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['userBusinessCard'];
    return $events;
  }

}