<?php

namespace Drupal\regportal_global\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\regportal_user\RegportalUserHelper;
use Drupal\user\Entity\User;

/**
 * Class Contacts.
 *
 * @Block(
 *   id = "regportal_contacts",
 *   admin_label = @Translation("Regportal Contacts"),
 *   category = @Translation("Custom Blocks"),
 * )
 */
class Contacts extends BlockBase implements ContainerFactoryPluginInterface  {

  /**
   * @var AccountInterface $account
   */
  protected $account;

  /**
   * The flag service.
   *
   * @var \Drupal\regportal_user\RegportalUserHelper
   */
  protected $regportalUser;

    /**
     * {@inheritdoc}
     */
    public function __construct(array $configuration,
                                $plugin_id,
                                $plugin_definition,
                                AccountInterface $account,
                                RegportalUserHelper $regportal_user) {
      parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->account = $account;
    $this->regportalUser = $regportal_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
                                array $configuration,
                                $plugin_id,
                                $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('regportal_user.regportal_user_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'phone' => $this->t('8(800)555-111-6'),
      'phone_desc' => $this->t('по России звонок бесплатный'),
      'email' => $this->t('sales@regportal.su'),
      'telegram' => $this->t('@Regportal'),
      'telegram_link' => $this->t('https://t.me/Regportal'),
      'order_call' => $this->t('Заказать бесплатный звонок'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['phone'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Телефон'),
      '#default_value' => $this->configuration['phone'],
      '#required' => TRUE,
    ];

    $form['phone_desc'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Текст под телефоном'),
      '#default_value' => $this->configuration['phone_desc'],
      '#required' => TRUE,
    ];

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#default_value' => $this->configuration['email'],
      '#required' => TRUE,
    ];

    $form['telegram'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Текст для Телеграм'),
      '#default_value' => $this->configuration['telegram'],
      '#required' => TRUE,
    ];

    $form['telegram_link'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Ссылка на Телеграм'),
      '#default_value' => $this->configuration['telegram_link'],
      '#required' => TRUE,
    ];

    $form['order_call'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Текст о заказе звонка'),
      '#default_value' => $this->configuration['order_call'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['phone']
      = $form_state->getValue('phone');
    $this->configuration['phone_desc']
      = $form_state->getValue('phone_desc');
    $this->configuration['email']
      = $form_state->getValue('email');
    $this->configuration['telegram']
      = $form_state->getValue('telegram');
    $this->configuration['telegram_link']
      = $form_state->getValue('telegram_link');
    $this->configuration['order_call']
      = $form_state->getValue('order_call');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $user_array = [];
    if (!$this->account->isAnonymous()) {
      $user = User::load($this->account->id());
      $user_array = [
        '#user_id' => $user->id(),
        '#user_full_name' => $user->field_user_visible_name->value,
        '#user_discount' => $user->field_user_discount->isEmpty() ? 0 : $user->field_user_discount->value,
        '#user_picture' => $this->regportalUser->getUserPictureUrl($user, 'user_avatar'),
        '#user_count_favorites' => $this->regportalUser->getCountFavorites($user),
      ];
    }


    $array = array_merge([
      '#theme' => 'regportal-contacts',
      '#phone' => $this->configuration['phone'],
      '#phone_desc' => $this->configuration['phone_desc'],
      '#email' => $this->configuration['email'],
      '#telegram' => $this->configuration['telegram'],
      '#telegram_link' => $this->configuration['telegram_link'],
      '#order_call' => $this->configuration['order_call'],
      '#cache' => [
        'max-age' => 0,
      ],
    ], $user_array);

    return $array;
  }

}
