<?php

namespace Drupal\regportal_global\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\block\Entity\Block;

/**
 * Class ContactsFooter.
 *
 * @Block(
 *   id = "regportal_contacts_footer",
 *   admin_label = @Translation("Regportal Contacts Footer"),
 *   category = @Translation("Custom Blocks"),
 * )
 */
class ContactsFooter extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = Block::load('regportalcontacts');

    if ($block) {
      $settings = $block->get('settings');

      return [
        '#theme' => 'regportal-contacts-footer',
        '#phone' => $settings['phone'],
        '#phone_desc' => $settings['phone_desc'],
        '#email' => $settings['email'],
        '#telegram' => $settings['telegram'],
        '#telegram_link' => $settings['telegram_link'],
        '#order_call' => $settings['order_call'],
      ];
    }


    return [];
  }

}
