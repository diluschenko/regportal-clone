<?php

namespace Drupal\regportal_global\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ContactsFooter.
 *
 * @Block(
 *   id = "regportal_copyright",
 *   admin_label = @Translation("Regportal Copyright"),
 *   category = @Translation("Custom Blocks"),
 * )
 */
class Copyright extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'copyright' => $this->t('© Copyright 2014 - 2017 ООО «РЕГПОРТАЛ.СУ»'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['copyright'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Copyright'),
      '#default_value' => $this->configuration['copyright'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['copyright']
      = $form_state->getValue('copyright');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    return ['#markup' => $this->configuration['copyright']];
  }

}
