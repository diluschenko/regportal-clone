<?php

namespace Drupal\regportal_global\EventSubscriber;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RegportalGlobalSubscriber implements EventSubscriberInterface {

  /**
   * Set page not found for non active nodes.
   */
  public function setPageNotFound(GetResponseEvent $event) {
    $route_name = $event->getRequest()->get('_route');

    if ('entity.node.canonical' !== $route_name) {
      return;
    }

    $node = $event->getRequest()->attributes->get('node');
    $bundles = ['director', 'bank_account'];
    if (!in_array($node->bundle(), $bundles)) {
      return;
    }

    throw new NotFoundHttpException();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['setPageNotFound'];
    return $events;
  }

}
