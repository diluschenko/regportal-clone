<?php
/**
 * @file
 * Contains \Drupal\okv_import\OkvedCsvFormatPreparer.
 */

namespace Drupal\regportal_import;

class OkvedCsvFormatPreparer extends AbstractCsvParser implements RegportalFormatPreparerInterface {

  /**
   * @const
   */
  const TYPE_TEXT = 'text';

  /**
   * @const
   */
  const TYPE_BOOL = 'bool';

  /**
   * @const
   */
  const TYPE_URL = 'url';

  /**
   * @param string $text
   *   A text for checking.
   *
   * @return array
   *   Returns array for further saving as Entity.
   */
  public function prepare($text) {
    $csv_data = $this->parse($text);

    if (!count($csv_data)) {
      return [];
    }

    $data = $this->prepareFields($csv_data);

    return $data;
  }

  /**
   * Prepare of the fields.
   *
   * @param array $csv_data
   *   An array of data.
   *
   * @return array
   *   Returns prepared data.
   */
  protected function prepareFields(array $csv_data) {
    $predefined_fields = json_decode(file_get_contents(drupal_get_path('module', 'regportal_import') . '/okv_import.csv_fields.json'));

    $fields_list = reset($csv_data);

    $header_fields_list = [];
    foreach ($fields_list as $field) {
      $header_fields_list[] = trim($field);
    }

    $entities = [];

    // Remove the header row from CSV file.
    unset($csv_data[0]);

    foreach ($csv_data as $csv_row) {
      $entity = [];
      foreach ($header_fields_list as $header_field_pos => $header_field) {
        $field_name = $predefined_fields->{$header_field}->field;
        $field_type = $predefined_fields->{$header_field}->type;
        $entity[$field_name] = $this->prepareValue($csv_row[$header_field_pos], $field_type);
      }

      $codes = explode('.', $entity['name']);

      if (count($codes) < 2 || (count($codes) == 2 && strlen($codes[1]) === 1)) {
        $entity['field_hidden'] = 1;
      }

      $entities[] = $entity;
    }

    return $entities;
  }

  /**
   * Prepare of the value.
   *
   * @param string $value
   *   A value for preparing.
   * @param string $type
   *   A type of the value.
   *
   * @return mixed
   *   Returns prepared value.
   */
  protected function prepareValue($value, $type) {
    switch ($type) {
      case self::TYPE_BOOL:
        return 'да' === strtolower($value);
        break;

      case self::TYPE_URL:
        if (0 !== strpos('http:\\', $value) && 0 !== strpos('https:\\', $value) && !empty($value)) {
          $value = 'http://' . $value;
        }
        return (string) $value;
        break;

      case self::TYPE_TEXT:
      default:
        return (string) $value;
    }
  }

}
