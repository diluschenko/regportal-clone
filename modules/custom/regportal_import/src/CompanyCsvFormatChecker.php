<?php
/**
 * @file
 * Contains \Drupal\regportal_import\CompanyCsvFormatChecker.
 */

namespace Drupal\regportal_import;

class CompanyCsvFormatChecker extends AbstractCsvParser implements RegportalFormatCheckerInterface {

  /**
   * @var array
   */
  protected $predefinedData;

  /**
   * @param string $text
   *   A text for checking.
   *
   * @return bool
   *   Returns result of the checking.
   */
  public function isCorrect($text) {
    $csv_data = $this->parse($text);

    if (!count($csv_data)) {
      drupal_set_message(t('Файл пуст'));
      return FALSE;
    }

    $fields_list = reset($csv_data);

    $trim_fields_list = [];
    foreach ($fields_list as $field) {
      $trim_fields_list[] = trim($field);
    }

    $check_fields_list = $this->checkFieldsList($trim_fields_list);

    // Remove the header row from CSV file.
    unset($csv_data[0]);

    return $check_fields_list;
  }

  /**
   * Checking list of the fields.
   *
   * @param array $fields
   *   An array of fields.
   *
   * @return bool
   *   Returns a checking result.
   */
  protected function checkFieldsList(array $fields) {
    $this->predefinedData = json_decode(file_get_contents(drupal_get_path('module', 'regportal_import') . '/company_import.csv_fields.json'));

    $predefined_fields = [];

    foreach ($this->predefinedData as $key => $row) {
      $predefined_fields[] = $key;
    }

    foreach ($predefined_fields as $key => $predefined_field) {
      $current = trim(str_replace(array("\r\n", "\r", "\n"), '',  strip_tags($fields[$key])));
      if ($predefined_field !== $current) {
        drupal_set_message(t('Проверьте последовательность столбцов в файле'), 'error');
        return FALSE;
      }
    }

    return TRUE;
  }
}
