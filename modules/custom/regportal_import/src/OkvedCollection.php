<?php
/**
 * @file
 * Contains \Drupal\regportal_import\OkvedCollection.
 */

namespace Drupal\regportal_import;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;


class OkvedCollection implements CollectionInterface {

  /**
   * @const
   */
  const ENTITY_MACHINE_NAME = 'taxonomy_term';

  /**
   * @const
   */
  const BUNDLE_MACHINE_NAME = 'okved';

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var SqlEntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->storage = $entityTypeManager->getStorage(self::ENTITY_MACHINE_NAME);
  }

  /**
   * Add new entity.
   *
   * @param array $values
   *   Values of an entity.
   *
   * @return EntityInterface
   */
  public function addEntity(array $values) {
    $values['vid'] = self::BUNDLE_MACHINE_NAME;
    $entity = $this->storage->create($values);
    $entity->save();

    return $entity;
  }

  /**
   * Truncate all entities.
   */
  public function truncateAllEntities() {
    $entities = $this->getAllEntities();
    $this->storage->delete($entities);
  }

  /**
   * Get all entities.
   *
   * @return array
   *   Returns an array of EntityInterface objects.
   */
  public function getAllEntities() {
    $ids = $this->storage->getQuery()->execute();

    return $this->storage->loadMultiple($ids);
  }

}
