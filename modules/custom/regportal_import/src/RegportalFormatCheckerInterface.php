<?php
/**
 * @file
 * Contains \Drupal\regportal_import\RegportalFormatCheckerInterface.
 */

namespace Drupal\regportal_import;

interface RegportalFormatCheckerInterface {

  /**
   * @param string $text
   *   A text for checking.
   *
   * @return bool
   *   Returns result of the checking.
   */
  public function isCorrect($text);

}
