<?php
/**
 * @file
 * Contains \Drupal\regportal_import\RegportalFormatPreparerInterface.
 */

namespace Drupal\regportal_import;

interface RegportalFormatPreparerInterface {

  /**
   * @param string $text
   *   A text for checking.
   *
   * @return array
   *   Returns array for further saving as Entity.
   */
  public function prepare($text);

}
