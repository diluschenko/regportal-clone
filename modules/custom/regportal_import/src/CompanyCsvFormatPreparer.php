<?php
/**
 * @file
 * Contains \Drupal\okv_import\CompanyCsvFormatPreparer.
 */

namespace Drupal\regportal_import;

use DateTime;
use Drupal\node\Entity\Node;

class CompanyCsvFormatPreparer extends AbstractCsvParser implements RegportalFormatPreparerInterface {

  /**
   * @const
   */
  const TYPE_TEXT = 'text';

  /**
   * @const
   */
  const TYPE_INT = 'integer';

  /**
   * @const
   */
  const TYPE_DATE = 'date';

  /**
   * @const
   */
  const TYPE_INTEGER = 'int';

  /**
   * @const
   */
  const REG_NUMBER_POS = 5;

  /**
   * @param string $text
   *   A text for checking.
   *
   * @return array
   *   Returns array for further saving as Entity.
   */
  public function prepare($text) {
    $csv_data = $this->parse($text);

    if (!count($csv_data)) {
      return [];
    }

    $data = $this->prepareFields($csv_data);

    return $data;
  }

  /**
   * Prepare of the fields.
   *
   * @param array $csv_data
   *   An array of data.
   *
   * @return array
   *   Returns prepared data.
   */
  protected function prepareFields(array $csv_data) {
    $predefined_fields = json_decode(file_get_contents(drupal_get_path('module', 'regportal_import') . '/company_import.csv_fields.json'));

    $fields_list = reset($csv_data);

    $header_fields_list = [];
    foreach ($fields_list as $field) {
      $header_fields_list[] = trim($field);
    }

    $entities = [];

    // Remove the header row from CSV file.
    unset($csv_data[0]);

    try {
      foreach ($csv_data as $key => $csv_row) {
        $entity = [];
        $is_error = FALSE;
        // Check a company in DB.
        $match = preg_match('/[^\d]+(\d{13})[^\d]+|[^\d]+(\d{13})$|[^\d]+(\d{15})[^\d]+|[^\d]+(\d{15})$/', $csv_row[self::REG_NUMBER_POS], $output);

        if ($match) {
          $reg_number = end($output);
          $entity['field_company_reg_number'] = $reg_number;
          $nid = $this->checkCompanyOnSite($reg_number);
        }
        else {
          drupal_set_message(
            t(
              'Поле ОГРН должно содержать 11 или 13 цифр в строке @number',
              ['@number' => $key + 1]
            ),
            'error'
          );

          continue;
        }

        // Check action.
        $action = mb_convert_case($csv_row[count($csv_row) - 1], MB_CASE_LOWER, "UTF-8");
        if ($nid) {
          $entity['nid'] = $nid;
          if ($action == 'удалить') {
            $entity['action'] = 'delete';
            $entities[] = $entity;
            continue;
          }
          else {
            $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
            if ($node->field_company_status->value == COMPANY_STATUS_SOLD_OUT) {
              continue;
            }
            $entity['action'] = 'update';
          }
        }
        else {
          if ($action == 'удалить') {
            drupal_set_message(
              t(
                'Компания в строке @number не может быть удалена, т.к. отсутствует в базе',
                ['@number' => $key + 1]
              ),
              'error'
            );
            continue;
          }
          $entity['action'] = 'create';
        }

        foreach ($header_fields_list as $header_field_pos => $header_field) {
          $field_name = $predefined_fields->{$header_field}->field;
          // Exclude some fields which was determined above.
          $fields = ['field_company_reg_number', 'action'];
          if (in_array($field_name, $fields)) {
            continue;
          }

          $field_type = $predefined_fields->{$header_field}->type;
          $required = $predefined_fields->{$header_field}->required;
          $value = trim($csv_row[$header_field_pos]);
          if ($required && empty($value)) {
            drupal_set_message(
              t(
                'Обязательное поле <strong>@field_name</strong> не содержит данных в <strong>@number</strong> строке',
                ['@field_name' => $header_field, '@number' => $key + 1]
              ),
              'error'
            );
            $is_error = TRUE;
            continue;
          }
          elseif (empty($value)) {
            $entity[$field_name] = NULL;
            continue;
          }

          switch ($field_name) {
            case 'field_company_okved':
              $entity[$field_name] = $this->prepareOkveds($value);
              break;

            case 'field_company_region':
              $entity[$field_name] = $this->prepareRegion($value);
              break;

            case 'field_company_bank_account':
              $entity[$field_name] = $this->prepareBankAccount($value);
              break;

            case 'field_company_director':
              $entity[$field_name] = $this->prepareDirector($value, $nid);
              break;

            case 'uid':
              $entity[$field_name] = $this->prepareUserId($value);
              if (empty($entity[$field_name])) {
                drupal_set_message(
                  t(
                    'Пользователь с именем <strong>@username</strong> не найден в базе. Строка: <strong>@number</strong>.',
                    ['@username' => $value, '@number' => $key + 1]
                  ),
                  'error'
                );
                $is_error = TRUE;
              }
              break;

            case 'field_company_status':
              $entity[$field_name] = self::getStatusId($value);
              break;

            default:
              $entity[$field_name] = $this->prepareValue($value, $field_type);
          }
        }

        if ($is_error) {
          continue;
        }

        $entities[] = $entity;
      }
    }
    catch (Exception $e) {
      \Drupal::logger('csv_prepare_company')->error($e->getMessage());
    }

    return $entities;
  }

  /**
   * Prepare of the value.
   *
   * @param string $value
   *   A value for preparing.
   * @param string $type
   *   A type of the value.
   *
   * @return mixed
   *   Returns prepared value.
   */
  protected function prepareValue($value, $type) {
    switch ($type) {
      case self::TYPE_DATE:
        $date = DateTime::createFromFormat('d.m.y', $value);
        return $date->format('Y-m-d');

      case self::TYPE_INT:
        return preg_replace('~\D+~','',$value);


      case self::TYPE_TEXT:
      default:
        return (string) $value;
    }
  }

  /**
   * Prepare OKVEDs.
   *
   * @param string $value
   *   A value for preparing.
   *
   * @return array
   *   Array of okveds.
   */
  protected function prepareOkveds($value) {
    $okveds = explode(',', $value);
    $target_ids = [];
    foreach ($okveds as $okved) {
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
        'name' => trim($okved),
        'vid' => 'okved',
      ]);
      $term = reset($terms);

      if(!empty($term)) {
        $target_ids[] = ['target_id' => $term->id()];
      }
      else {
        drupal_set_message(
          t(
            'Оквед <strong>@okved</strong> не найден в базе',
            ['@okved' => $okved]
          ),
          'error'
        );
      }
    }

    return count($target_ids) ? $target_ids : NULL;
  }

  /**
   * Prepare region.
   *
   * @param string $value
   *   A value for preparing.
   *
   * @return array
   *   Array of term reference.
   */
  protected function prepareRegion($value) {
    $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
      'name' => $value,
      'vid' => 'regions',
    ]);
    $term = reset($terms);

    return !empty($term) ? ['target_id' => $term->id()] : NULL;
  }

  /**
   * Prepare bank account.
   *
   * @param string $value
   *   A value for preparing.
   *
   * @return array
   *   Array of bank accounts.
   */
  protected function prepareBankAccount($value) {
    if ($value == 'Без счёта') {
      return NULL;
    }

    $banks = explode(',', $value);
    $target = [];
    foreach ($banks as $bank) {
      $bank = str_replace('+карта', '', $bank, $count);

      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties([
        'name' => trim($bank),
        'vid' => 'banki',
      ]);
      $term = reset($terms);
      if (!$term) {
        continue;
      }

      $query = \Drupal::entityQuery('node')
        ->condition('type', 'bank_account')
        ->condition('field_bank', $term->id());
      if ($count) {
        $query->condition('field_bank_account_with_account', 1);
      }
      else {
        $query->condition('field_bank_account_with_account', 1, '!=');
      }

      $nids = $query->execute();

      if(!empty($nids)) {
        $target[] = ['target_id' => reset($nids)];
      }
    }

    return !empty($target) ? $target : NULL;
  }

  /**
   * Prepare bank account.
   *
   * @param string $value
   *    A value for preparing.
   * @param int $company_nid
   *    The company nid
   *
   * @return array
   *   Array of bank accounts.
   */
  protected function prepareDirector($value, $company_nid) {
    if ($company_nid) {
      $node = \Drupal::entityTypeManager()->getStorage('node')->load($company_nid);
      if (!$node->field_company_director->isEmpty()) {
        $director = $node->field_company_director->referencedEntities()[0];
      }
    }

    if (isset($director)) {
      if ($director->label() == $value) {
        return ['target_id' => $director->id()];
      }
      elseif ($director->label() != $value && $director->label() == 'Обязательная смена') {
        $director->delete();
      }
    }

    if ($value != 'Обязательная смена') {
      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        'title' => $value,
      ]);
    }

    if (empty($nids) || $value == 'Обязательная смена') {
      $node = Node::create([
        'type'        => 'director',
        'title'       => $value,
      ]);
      $node->save();
      $nid = $node->id();
    }
    else {
      $nid = reset($nids)->id();
    }

    return ['target_id' => $nid];
  }

  /**
   * Prepare user id.
   *
   * @param string $value
   *   A value for preparing.
   *
   * @return int
   *   User ID.
   */
  protected function prepareUserId($value) {
    $users = \Drupal::entityTypeManager()->getStorage('user')->loadByProperties([
      'name' => $value,
    ]);
    $user = reset($users);

    return $user ? $user->id() : NULL;
  }

  /**
   * Check if changed status.
   *
   * @param int $nid
   *    The company nid.
   * @param string $status
   *    The status.
   *
   * @return bool
   *   Should we create ne paragraph or no.
   */
  protected function sameCompanyStatus($nid, $status) {
    if (!$nid) {
      return FALSE;
    }
    $node = \Drupal::entityTypeManager()->getStorage('node')->load($nid);
    if ($node->field_company_status->isEmpty()) {
      return FALSE;
    }

    $status = self::getStatusId($status);
    $current_statuses = $node->field_company_status->referencedEntities();
    $current_status = end($current_statuses);

    return $status === (int) $current_status->field_cs_status->value ? TRUE : FALSE;
  }

  /**
   * Check company it the DB.
   *
   * @param string $reg_number
   *   Register number.
   *
   * @return int
   *  NID.
   */
  protected function checkCompanyOnSite($reg_number) {
    $query = \Drupal::entityQuery('node')
      ->condition('type', 'company')
      ->condition('field_company_reg_number', $reg_number);

    $nids = $query->execute();

    return count($nids) ? reset($nids) : 0;
  }

  public static function getStatusId($status) {
    $statuses = [
      1 => 'в продаже',
      2 => 'забронирована',
      3 => 'продана',
      4 => 'продажа приостановлена',
      5 => 'производство - выпуск ЭЦП',
      6 => 'производство - подготовка документов',
      7 => 'производство - оплата пошлины',
      8 => 'производство - подача на регистрауию',
      9 => 'производство - ждем регистрации',
      10 => 'производство - получение',
      11 => 'производство - отказ от регистации',
      12 => 'производство - исправление ошибки',
      13 => 'производство - на открытии счета',
    ];

    return array_search($status, $statuses);
  }

}
