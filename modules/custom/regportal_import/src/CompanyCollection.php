<?php
/**
 * @file
 * Contains \Drupal\regportal_import\CompanyCollection.
 */

namespace Drupal\regportal_import;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\node\Entity\Node;


class CompanyCollection implements CollectionInterface {

  /**
   * @const
   */
  const ENTITY_MACHINE_NAME = 'node';

  /**
   * @const
   */
  const BUNDLE_MACHINE_NAME = 'company';

  /**
   * @var EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * @var SqlEntityStorageInterface
   */
  protected $storage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
    $this->storage = $entityTypeManager->getStorage(self::ENTITY_MACHINE_NAME);
  }

  /**
   * Add new entity.
   *
   * @param array $values
   *   Values of an entity.
   *
   * @return EntityInterface
   */
  public function addEntity(array $values) {
    try {
      $values['type'] = self::BUNDLE_MACHINE_NAME;
      $node = Node::create($values);
      $node->save();

      return $node;
    }
    catch (Exception $e) {
      \Drupal::logger('csv_create_company')->error($e->getMessage());
    }
  }

  /**
   * Truncate all entities.
   */
  public function truncateAllEntities() {
    $entities = $this->getAllEntities();
    $this->storage->delete($entities);
  }

  /**
   * Get all entities.
   *
   * @return array
   *   Returns an array of EntityInterface objects.
   */
  public function getAllEntities() {
    $ids = $this->storage->getQuery()->execute();

    return $this->storage->loadMultiple($ids);
  }

  /**
   * Delete node.
   *
   * @param array $values
   *   Values of an entity.
   */
  public function deleteEntity($values) {
    try {
      $node = $this->storage->load($values['nid']);
      $node->delete();
    }
    catch (Exception $e) {
      \Drupal::logger('csv_delete_company')->error($e->getMessage());
    }
  }

  /**
   * Update node.
   *
   * @param array $values
   *   Values of an entity.
   */
  public function updateEntity($values) {
    try {
      $node = $this->storage->load($values['nid']);
      foreach ($values as $key => $value) {
        $node->{$key} = $value;
      }
      $node->save();
    }
    catch (Exception $e) {
      \Drupal::logger('csv_update_company')->error($e->getMessage());
    }
  }

}
