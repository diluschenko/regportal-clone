<?php
/**
 * @file
 * Contains \Drupal\regportal_import\CollectionInterface.
 */

namespace Drupal\regportal_import;

use Drupal\Core\Entity\EntityInterface;

interface CollectionInterface {

  /**
   * Add new entity.
   *
   * @param array $values
   *   Values of an entity.
   *
   * @return EntityInterface
   */
  public function addEntity(array $values);

  /**
   * Truncate all entities.
   */
  public function truncateAllEntities();

  /**
   * Get all entities.
   *
   * @return array
   *   Returns an array of EntityInterface objects.
   */
  public function getAllEntities();

}
