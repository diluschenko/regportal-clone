<?php
/**
 * @file
 * Contains \Drupal\regportal_import\OkvedCsvFormatChecker.
 */

namespace Drupal\regportal_import;

class OkvedCsvFormatChecker extends AbstractCsvParser implements RegportalFormatCheckerInterface {

  /**
   * @param string $text
   *   A text for checking.
   *
   * @return bool
   *   Returns result of the checking.
   */
  public function isCorrect($text) {
    $csv_data = $this->parse($text);

    if (!count($csv_data)) {
      return FALSE;
    }

    $fields_list = reset($csv_data);

    $trim_fields_list = [];
    foreach ($fields_list as $field) {
      $trim_fields_list[] = trim($field);
    }

    $check_fields_list = $this->checkFieldsList($trim_fields_list);

    return $check_fields_list;
  }

  /**
   * Checking list of the fields.
   *
   * @param array $fields
   *   An array of fields.
   *
   * @return bool
   *   Returns a checking result.
   */
  protected function checkFieldsList(array $fields) {
    $predefined_data = json_decode(file_get_contents(drupal_get_path('module', 'regportal_import') . '/okv_import.csv_fields.json'));

    $predefined_fields = [];

    foreach ($predefined_data as $key => $row) {
      $predefined_fields[] = $key;
    }

    foreach ($predefined_fields as $key => $predefined_field) {
      if ($predefined_field !== $fields[$key]) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
