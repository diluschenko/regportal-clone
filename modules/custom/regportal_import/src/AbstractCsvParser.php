<?php
/**
 * @file
 * Contains \Drupal\regportal_import\AbstractCsvParser.
 */

namespace Drupal\regportal_import;

abstract class AbstractCsvParser {

  /**
   * @param string $text
   *   A text for parsing.
   *
   * @return array
   *   Returns parsed CSV data.
   */
  public function parse($text) {
    $text = preg_replace_callback('/;"(.*?)"/ims', function($matches) {
      $modified = ';"' . str_replace(["\r", "\n"], ['<br>', '<br>'], $matches[1]) . '"';
      return $modified;
    }, $text);

    $csv_rows = preg_split("/\r\n|\n|\r/", $text);

    $csv_data = [];

    foreach ($csv_rows as $csv_row) {
      $csv_row = str_replace('<br>', "\n", $csv_row);
      $csv_data[] = str_getcsv($csv_row, ";");
    }

    return $csv_data;
  }

}
