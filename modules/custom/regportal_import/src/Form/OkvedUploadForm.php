<?php
/**
 * @file
 * Contains \Drupal\regportal_import\Form\OkvedUploadForm.
 */

namespace Drupal\regportal_import\Form;

use Drupal\Core\File\FileSystem;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\regportal_import\CollectionInterface;
use Drupal\regportal_import\RegportalFormatCheckerInterface;
use Drupal\regportal_import\RegportalFormatPreparerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Okved Upload Form.
 */
class OkvedUploadForm extends FormBase {

  /**
   * @const
   */
  const FILE_ENTITY_MACHINE_NAME = 'file';

  /**
   * @var FileSystem
   */
  protected $fileSystem;

  /**
   * @var RegportalFormatCheckerInterface
   */
  protected $formatChecker;


  /**
   * @var RegportalFormatPreparerInterface
   */
  protected $formatPreparer;

  /**
   * @var CollectionInterface
   */
  protected $okvedCollection;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    FileSystem $fileSystem,
    RegportalFormatCheckerInterface $formatChecker,
    RegportalFormatPreparerInterface $formatPreparer,
    CollectionInterface $okvedCollection
  ) {
    $this->fileSystem = $fileSystem;
    $this->formatChecker = $formatChecker;
    $this->formatPreparer = $formatPreparer;
    $this->okvedCollection = $okvedCollection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('file_system'),
      $container->get('regportal_import.okved_format_checker'),
      $container->get('regportal_import.okved_format_preparer'),
      $container->get('regportal_import.okved_collection')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'okv_import_upload_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['field_okveddata_upload'] = [
      '#type' => 'file',
      '#title' => $this->t('Загрузка файла'),
      '#description' => $this->t('Кодировка файла должна быть в UTF8.'),
    ];

    $form['field_okveddata_import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Импортировать'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $file_collection = file_save_upload('field_okveddata_upload', [
      'file_validate_extensions' => ['csv'],
    ]);

    $file = reset($file_collection);

    if (is_null($file_collection) || FALSE === $file) {
      $form_state->setErrorByName(
        'field_okveddata_upload',
        $this->t('Файл должен быть с расширением csv.')
      );
      return;
    }

    $file_uri = $file->getFileUri();
    $realpath = $this->fileSystem->realpath($file_uri);

    $csv_content = file_get_contents($realpath);

    if (!$this->formatChecker->isCorrect($csv_content)) {
      $form_state->setErrorByName(
        'field_okveddata_upload',
        $this->t('В файле некорректные данные.')
      );
      return;
    }

    $form_state->setValue('csv', $csv_content);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->okvedCollection->truncateAllEntities();

    $csv_content = $form_state->getValue('csv');

    $entities = $this->formatPreparer->prepare($csv_content);

    foreach ($entities as $entity) {
      $this->okvedCollection->addEntity($entity);
    }

    drupal_set_message(
      $this->t(
        'Данные загружены'
      )
    );
  }

}
