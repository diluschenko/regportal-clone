<?php

/**
 * @file
 * Contains regportal views hooks.
 */

/**
 * Implements hook_views_data_alter().
 */
function regportal_views_views_data_alter(array &$data) {
  $data['node__field_company_bank_account']['bank_account_filter'] = array(
    'title' => t('Без счета'),
    'group' => t('Content'),
    'filter' => array(
      'title' => t('Без счета'),
      'help' => t('Выборка фирм без счета.'),
      'field' => 'field_company_bank_account_target_id',
      'id' => 'bank_account_filter',
    ),
  );

  $data['node']['company_price_with_discount'] = array(
    'title' => t('Price with discount'),
    'field' => array(
      'title' => t('Price with discount'),
      'help' => t("Company's price with discount"),
      'id' => 'company_price_with_discount',
    ),
  );

  $data['node']['order_company_price_with_discount'] = array(
    'title' => t('Price with discount in the order'),
    'field' => array(
      'title' => t('Price with discount in the order'),
      'help' => t("Company's price with discoun in the ordert"),
      'id' => 'order_company_price_with_discount',
    ),
  );

  $data['node']['order_company_discount'] = array(
    'title' => t('Static discount for all firms'),
    'field' => array(
      'title' => t('Static discount for all firms'),
      'help' => t("Discount by default 200"),
      'id' => 'order_company_discount',
    ),
  );

  $data['node']['order_end_date_discount'] = array(
    'title' => t('End date discount'),
    'field' => array(
      'title' => t('End date discount'),
      'help' => t("Last day for discount"),
      'id' => 'order_end_date_discount',
    ),
  );
}