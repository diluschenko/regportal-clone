<?php

/**
 * @file
 * Contains regportal views module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\query\QueryPluginBase;
use Drupal\Component\Utility\Tags;
use Drupal\Core\Database\Database;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Implements hook_form_alter().
 */
function regportal_views_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'views_exposed_form' &&  $form_state->getStorage()['view']->id() == 'companies') {

    if (isset($form['field_company_okved_target_id'])) {
      $form['field_company_okved_target_id']['#type'] = 'textfield';
      $form['field_company_okved_target_id']['#autocomplete_route_name'] = 'regportal_views.okved_autocomplete';
      $form['field_company_okved_target_id']['#autocomplete_route_parameters'] = [
        'field_name' => 'field_company_okved_target_id',
        'count' => 10,
      ];
      $form['field_company_okved_target_id']['#title_display'] = 'invisible';
      $form['field_company_okved_target_id']['#placeholder'] = t('ОКВЭД (начните ввод)');
    }

    if (isset($form['uid'])) {
      $form['uid']['#type'] = 'textfield';
      $form['uid']['#autocomplete_route_name'] = 'regportal_views.uid_autocomplete';
      $form['uid']['#autocomplete_route_parameters'] = [
        'field_name' => 'uid',
        'count' => 10,
      ];
      $form['uid']['#title_display'] = 'invisible';
      $form['uid']['#placeholder'] = t('Регистратор (начните ввод)');
    }

    if (isset($form['bank_account_filter'])) {
      $form['bank_account_filter']['#type'] = 'radios';
      $form['bank_account_filter']['#options']['All'] = t('Все Фирмы');
    }

    if (isset($form['title'])) {
      $form['title']['#title_display'] = 'invisible';
      $form['title']['#placeholder'] = t('Название фирмы');
    }

    if (isset($form['field_company_region_target_id'])) {
      $form['field_company_region_target_id']['#title_display'] = 'invisible';
      $form['field_company_region_target_id']['#attributes']['data-placeholder'] = t('Регион');

      $form['field_company_region_target_id']['#options'] = _regportal_views_get_available_terms('field_company_region', $form['field_company_region_target_id']['#options']);
    }

    if (isset($form['field_company_date_value'])) {
      $form['field_company_date_value']['#title_display'] = 'invisible';
      $form['field_company_date_value']['#placeholder'] = '01.01.2017';
    }

    if (isset($form['field_company_date_value_1'])) {
      $form['field_company_date_value_1']['#title_display'] = 'invisible';
      $form['field_company_date_value_1']['#placeholder'] = '01.10.2017';
    }

    if (isset($form['field_company_price_value'])) {
      $form['field_company_price_value']['#title_display'] = 'invisible';
      $form['field_company_price_value']['#placeholder'] = '20000';
    }

    if (isset($form['field_company_price_value_1'])) {
      $form['field_company_price_value_1']['#title_display'] = 'invisible';
      $form['field_company_price_value_1']['#placeholder'] = '25000';
    }

    if (isset($form['field_bank_target_id'])) {
      $form['field_bank_target_id']['#title_display'] = 'invisible';
      $form['field_bank_target_id']['#attributes']['data-placeholder'] = t('Банк');
      $form['field_bank_target_id']['#options'] = _regportal_views_get_available_banks($form['field_bank_target_id']['#options']);
    }

    if (isset($form['field_company_reg_number_value'])) {
      $form['field_company_reg_number_value']['#title_display'] = 'invisible';
      $form['field_company_reg_number_value']['#placeholder'] = t('ОГРН');
    }

    if (isset($form['field_company_tax_number_value'])) {
      $form['field_company_tax_number_value']['#title_display'] = 'invisible';
      $form['field_company_tax_number_value']['#placeholder'] = t('Номер налоговой (через запятую)');

      $options = [
        0 => 'только с этими ИФНС',
        1 => 'только НЕ с этими ИФНС',
      ];
      $form['without_ifns'] = [
        '#type' => 'radios',
        '#default_value' => 0,
        '#options' => $options,
      ];
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function regportal_views_views_query_alter(ViewExecutable $view, QueryPluginBase $query) {
  if ($view->id() == 'companies' && $view->current_display != 'block_updates') {
    if (!empty($input = $view->exposed_data['field_company_okved_target_id'])) {
      $okveds = Tags::explode($input);
      $tids = [];
      foreach ($okveds as $okved) {
        $condition = '=';
        if (strpos($okved, '*') !== FALSE) {
          $okved = str_replace('*', '%', $okved);
          $condition = 'like';
        }
        $terms = \Drupal::entityTypeManager()
          ->getStorage('taxonomy_term')
          ->getQuery()
          ->condition('vid', 'okved')
          ->condition('name', trim($okved), $condition)
          ->execute();

        if (count($terms)) {
          foreach ($terms as $term) {
            $tids[] = $term;
          }
        }
      }

      $configuration = array(
        'type'       => 'INNER',
        'table'      => 'node__field_company_okved',
        'field'      => 'entity_id',
        'left_table' => 'node_field_data',
        'left_field' => 'nid',
        'operator'   => '=',
      );

      $join = \Drupal\views\Views::pluginManager('join')
        ->createInstance('standard', $configuration);
      $rel = $query->addRelationship('node__field_company_okved', $join, 'node_field_data');
      $query->addTable('node__field_company_okved', $rel, $join, 'node__field_company_okved');
      $query->addWhere('', 'node__field_company_okved.field_company_okved_target_id', $tids, 'IN');
    }

    // Check if set user filter.
    if (isset($view->exposed_data['uid']) && !empty($input = $view->exposed_data['uid'])) {
      $match = NULL;
      if (preg_match("/.+\s\(([^\)]+)\)/", $input, $matches)) {
        $match = $matches[1];
      }

      foreach ($query->where[1]['conditions'] as &$condition) {
        if ($condition['field'] == 'node_field_revision.uid') {
          $condition['value'] = $match;
        }
      }
    }

    // Check ifns.
    if (isset($view->exposed_data['field_company_tax_number_value'])) {
      foreach ($query->where[1]['conditions'] as &$condition) {
        if ($condition['field'] == 'node__field_company_tax_number.field_company_tax_number_value') {
          $values = Tags::explode($condition['value']);
          $condition['value'] = $values;
          if ($view->exposed_data['without_ifns']) {
            $condition['operator'] = 'NOT IN';
          }
          else {
            $condition['operator'] = 'IN';
          }
        }
      }
    }
  }

  if ($view->id() == 'orders' && $view->current_display == 'block_order_discount') {
    $date = new DrupalDateTime('-7 days midnight');

    $query->addWhere('', 'node__field_order_status_date.field_order_status_date_value', strtotime($date->format(DATETIME_DATETIME_STORAGE_FORMAT)), '>=');
  }
}

/**
 * Implements hook_views_pre_render().
 */
function regportal_views_views_pre_render(ViewExecutable $view) {
  $displays = ['data_export', 'data_export_1', 'data_export_2', 'data_export_3'];
  $current_user = \Drupal::currentUser();
  if ($view->id() == 'xls' && $view->current_display == 'data_export_1') {
    foreach ($view->result as $value) {
      if (!$current_user->hasPermission('view company secret data')) {
        $value->_entity->set('field_company_reg_number', 'xxxxxxxxxxx');
        $value->_entity->set('field_company_director', '');
      }
    }
  }
  elseif ($view->id() == 'companies' && in_array($view->current_display, $displays)) {
    foreach ($view->result as $value) {
      if (!$current_user->hasPermission('view company secret data')) {
        $value->node__field_company_reg_number_field_company_reg_number_valu = 'xxxxxxxxxxx';
        $value->node__field_company_director_field_company_director_target_i = '';
      }
    }
  }
}


/**
 * Get available regions.
 *
 * @param string $field_name
 *   The field name.
 * @param array $options
 *   The options for a field.
 *
 * @return array
 *   The options.
 */
function _regportal_views_get_available_terms($field_name, $options) {
  $con = Database::getConnection();

  $query = $con->select('node__' . $field_name, 'f')
    ->fields('f', [$field_name . '_target_id'])
    ->distinct();
  $query->leftJoin( 'node__field_company_status', 's', 's.entity_id = f.entity_id');

  $query->condition('s.field_company_status_value', COMPANY_STATUS_ON_SALE);
  $result = $query->execute()->fetchAllKeyed(0, 0);

  return array_intersect_key($options, $result);
}

/**
 * Get available regions.
 *
 * @param array $options
 *   The options for a field.
 *
 * @return array
 *   The options.
 */
function _regportal_views_get_available_banks($options) {
  $con = Database::getConnection();

  $query = $con->select('node__field_bank', 'f')
    ->fields('f', ['field_bank_target_id'])
    ->distinct();
  $query->leftJoin( 'node__field_company_bank_account', 'b', 'b.field_company_bank_account_target_id = f.entity_id');
  $query->leftJoin( 'node__field_company_status', 's', 's.entity_id = b.entity_id');

  $query->condition('s.field_company_status_value', COMPANY_STATUS_ON_SALE);
  $result = $query->execute()->fetchAllKeyed(0, 0);

  return array_intersect_key($options, $result);
}
