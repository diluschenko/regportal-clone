<?php

namespace Drupal\regportal_views\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Tags;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\Database\Database;

class AutocompleteController extends ControllerBase {

  public function okvedAutocomplete(Request $request, $field_name, $count) {
    $results = [];
    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = Unicode::strtolower(array_pop($typed_string));

      if (mb_strlen($typed_string) > 2) {
        if (ctype_digit(substr($typed_string, 0, 1))) {
          $search_by_field = 't.name';
        }
        else {
          $search_by_field = 'okv.field_okved_text_value';
        }

        $con = Database::getConnection();

        $query = $con->select('taxonomy_term_field_data', 't')
          ->fields('t', ['name']);
        $query->addField('okv', 'field_okved_text_value');
        $query->join('taxonomy_term__field_okved_text', 'okv', 'okv.entity_id = t.tid');
        $query->condition($search_by_field, "%" . $query->escapeLike($typed_string) . "%", 'LIKE');

        $query->addExpression("
        CASE
          WHEN $search_by_field like '" . $query->escapeLike($typed_string) . "%' THEN 0
          WHEN $search_by_field like '% %" . $query->escapeLike($typed_string) . "% %' THEN 1
          WHEN $search_by_field like '%" . $query->escapeLike($typed_string) . "' THEN 2
          ELSE 3
        END", 'name2');

        $query->orderBy('name2', 'ASC');
        $query->orderBy('t.name', 'ASC');

        $query->range(0, 500);
        $result = $query->execute()->fetchAll();

        foreach ($result as $item) {
          $code = preg_replace('/(' . $typed_string . ')/iu', '<span>$1</span>', $item->name);
          $name = preg_replace('/(' . $typed_string . ')/iu', '<span>$1</span>', $item->field_okved_text_value);

          $results[] = [
            'value' => $item->name,
            'label' => '<span class="okved-item">' . $code . ' ' . $name . '</span>',
          ];
        }
      }
    }

    return new JsonResponse($results);
  }

  public function uidAutocomplete(Request $request, $field_name, $count) {
    $results = [];

    // Get the typed string from the URL, if it exists.
    if ($input = $request->query->get('q')) {
      $typed_string = Tags::explode($input);
      $typed_string = Unicode::strtolower(array_pop($typed_string));

      $con = Database::getConnection();

      $query = $con->select('user__field_user_visible_name', 'u')
        ->fields('u', ['entity_id', 'field_user_visible_name_value'])
        ->distinct();

      $query->leftJoin( 'node_field_data', 'n', 'n.uid = u.entity_id');
      $query->leftJoin( 'node__field_company_status', 'nf', 'nf.entity_id = n.nid');

      if (mb_strlen($typed_string) > 1) {
        $query->condition('u.field_user_visible_name_value', "%" . $query->escapeLike($typed_string) . "%", 'LIKE');
      }
      else {
        $query->condition('u.field_user_visible_name_value', $query->escapeLike($typed_string) . "%", 'LIKE');
      }

      $query->condition('nf.field_company_status_value', COMPANY_STATUS_ON_SALE);
      $query->orderBy('u.field_user_visible_name_value', 'ASC');

      $query->range(0, $count);
      $result = $query->execute()->fetchAll();

      foreach ($result as $item) {
        $name = preg_replace('/(' . $typed_string . ')/iu', '<span>$1</span>', $item->field_user_visible_name_value);

        $results[] = [
          'value' =>  $item->field_user_visible_name_value . ' (' . $item->entity_id . ')',
          'label' => '<span class="okved-item">' . $name . '</span>',
        ];
      }
    }

    return new JsonResponse($results);
  }
}
