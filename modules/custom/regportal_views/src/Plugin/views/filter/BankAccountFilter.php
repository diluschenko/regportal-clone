<?php

/**
 * @file
 * Definition of Drupal\regportal_views\Plugin\views\filter\BankAccountFilter.
 */

namespace Drupal\regportal_views\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Filters if isset bank account.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("bank_account_filter")
 */
class BankAccountFilter extends FilterPluginBase {

  protected function valueForm(&$form, FormStateInterface $form_state) {
    $options = [
      1 => $this->t('Только без счетов'),
      2 => $this->t('Только со счетами'),
    ];

    $form['value'] = [
      '#type' => 'radios',
      '#title' => $this->t('Фирмы:'),
      '#default_value' => $this->value,
      '#options' => $options,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    if (count($this->value)) {
      $this->ensureMyTable();

      if ((int)$this->value[0] === 1) {
        $where = "$this->tableAlias.$this->realField IS NULL";
      }
      else {
        $where = "$this->tableAlias.$this->realField IS NOT NULL";
      }

      $this->query->addWhereExpression($this->options['group'], $where);
    }
  }
}
