<?php

/**
 * @file
 * Definition of Drupal\regportal_views\Plugin\views\LastDayDiscount
 */

namespace Drupal\regportal_views\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Field handler for company price with discount.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("order_end_date_discount")
 */
class LastDayDiscount extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    return parent::defineOptions();
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $order = $values->_entity;
    if ($order->bundle() == 'order' && !$order->field_order_status_date->isEmpty()) {
      $date = $order->field_order_status_date->value;

      return date('d.m.Y', strtotime('+7 days', $date));
    }

    return NULL;
  }
}