<?php

/**
 * @file
 * Definition of Drupal\d8views\Plugin\views\field\NodeTypeFlagger
 */

namespace Drupal\regportal_views\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\user\Entity\User;

/**
 * Field handler for company price with discount.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("company_price_with_discount")
 */
class PriceWithDiscount extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    return parent::defineOptions();
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    $company = $values->_entity;

    if ($company->bundle() == 'company' && !$company->field_company_price->isEmpty()) {
      $user = User::load(\Drupal::currentUser()->id());
      $discount = !$user->field_user_discount->isEmpty() ? $user->field_user_discount->value : 0;
      return number_format($company->field_company_price->value - $discount, 0, '.', ' ');
    }

    return NULL;
  }
}