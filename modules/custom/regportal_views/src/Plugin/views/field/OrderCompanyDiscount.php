<?php

/**
 * @file
 * Definition of Drupal\regportal_views\Plugin\views\OrderCompanyDiscount
 */

namespace Drupal\regportal_views\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Order company discount.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("order_company_discount")
 */
class OrderCompanyDiscount extends FieldPluginBase {

  /**
   * @{inheritdoc}
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options
   * @return array
   */
  protected function defineOptions() {
    return parent::defineOptions();
  }

  /**
   * @{inheritdoc}
   */
  public function render(ResultRow $values) {
    return ORDER_DISCOUNT;
  }
}