# Theme Template
Libsass & Gulp edition (for Drupal 7/8).

This theme-template features a set of pre-defined (gulp-based-) tasks to provide an efficient front-end-workflow. These tasks include sass-compiling, sass- & js-linting, image-minify and file-concatenation amongst others.

The basic idea is to provide a common setup, that should not be altered, with an additional config that offers the 
opportunity to customize the setup.

*This template does not include any drupal-theme-specific files (.info, .theme etc).*


## Install
`$ npm install` should take care of all your needs.

### Dependencies
- [NodeJS](https://nodejs.org/) ([how to install](https://nodejs.org/en/download/package-manager/))
- [npm](https://www.npmjs.com/) ([how to install](https://docs.npmjs.com/getting-started/installing-node))
- [Bower](https://bower.io/) ([how to install](https://bower.io/#install-bower))

### Post install
In some cases you might get "segmentation errors" when using drush after the install, due to some installed node-modules featuring .info files. This can be helped by running `$ sh npm-post.sh`, a script that renames all ".info"- files inside the node-modules folder to ".inf0", from the template-root-folder.



## General workflow


### Tasks
The most useful and used tasks:
- `$ gulp build`: clean any pre-existing generated files and re-generate them.
- `$ gulp` (or `$ gulp serve`): start a local server that injects updated files (css) and does page-reloads automatically on file-changes (js).
- `$ gulp lib`: clean the installed lib-files and copy all configured lib-files.
- `$ gulp test`: run sass-linting and js-hinting.

For development you might also need these:
- `$ gulp css:vendor`: concat the defined third-party css-files.
- `$ gulp js:vendor`: concat the defined third-party js-files.

For distribution:
- `$ gulp dist`: run `build` and remove all development files from the theme. this __removes all files__ from the theme that are not needed for the production environment.



## Package
package.json setup.

### Third-party content
Libraries like angular and others can be installed using npm (the preferred choice) or bower.

### Install packages without package.json
In some cases you might want to install a package that does not bring a setup for npm or bower. Here [napa](https://github.com/shama/napa) can be your friend, like this:
````
"scripts": {
  "install": "napa balsamiq/jquery-zeroclipboard:jquery-zeroclipboard"
}
````



## Tasks & Helper


### Images
- image-maps for sass
- [spriting](https://github.com/twolfson/gulp.spritesmith)
- minify for sprites and images


### Sass
- (helper) [color-naming](http://chir.ag/projects/name-that-color/)
- (info) [atomic web design](http://bradfrost.com/blog/post/atomic-web-design/)
- (task) [autoprefixer](https://github.com/postcss/autoprefixer)
- (task) [sass-linting](https://github.com/sasstools/sass-lint)
    - [config-options](https://github.com/sasstools/sass-lint/blob/master/lib/config/sass-lint.yml)
    - [sample config](https://github.com/sasstools/sass-lint/blob/master/docs/sass-lint.yml)
    
#### Automatic feature-detection for Modernizr
(... in case it's not detected by default) there are two ways to add the feature detection.

1. add the feature-test using the `gulp_config.json`
1. add a comment to the scss file naming the feature eg: `// Modernizr.objectfit`



### JS
- (info) [babel](https://babeljs.io/)
- (task) [gulp-babel](https://www.npmjs.com/package/gulp-babel)
    - [presents](https://babeljs.io/docs/plugins/#presets)

#### Linting
- (info) [ES Lint](http://eslint.org/docs/developer-guide/)
- (task) [gulp-eslint](https://github.com/adametry/gulp-eslint)



## Todos
- image sprite
    - prevent empty folders to be created
- propper readme
- check https://github.com/frontendfriends/gulp-combine-mq
- check https://www.npmjs.com/package/gulp-plumber (later)
- check theme as submodule https://subfictional.com/fun-with-git-submodules/
- check for deprecated packages



## Changelog
- 24.03.17
    - adjusted js-lint allowed usage of dangling underscore
- 17.03.17
    - fixed image-map and -sprite template usage
    - properly adjusted modernizr-setup to add testes form config
    - adjusted dist:clean task
- 16.03.17
    - moved from js-hint to es-lint
    - added [babel](https://babeljs.io/) to convert scripts to es5 (if needed)
- 05.03.17
    - added custom [modernizr](https://modernizr.com/) creation
- 14.11.16
    - added image spriting support
    - updated sass-linting
- 19.09.16
    - checked style-lint (feature/gulp-stylelint)
- 19.07.16
    - adjusted file-naming for concatenated and custom files using [gulp-rename](https://github.com/hparra/gulp-rename)
    - ported config
    - added `$ gulp dist:clean` -task and config for distribution
    - added `$ gulp dist` task
    - added clean config-option
    - added source config
    - updated readme
- 13.07.16
    - fixed import of breakpoint and normalize ([info](http://stackoverflow.com/a/33588202/4400555))
    - removed image-map helper template
- 06.07.16
    - updates sass-lint-config
    - added (wip) read-me
    - added useful sass-files
    - updated npm-post to show adjusted files
