'use strict';

(($) => {
  Drupal.behaviors.bagView = {
    attach(context) {
      const bag = $('.view-display-id-bag', context);

      function calculateSum() {
        let totalPrice = 0;

        $('.price', bag)
          .each((i, el) => {
            const price = $(el);

            totalPrice += parseInt(price.text().replace(/[^0-9]/g, ''), 10);
          });

        $('.total-section-price', bag).text(totalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
      }

      calculateSum();

      $('.flag-bag a', bag)
        .once('bag_unflag')
        .each((i, el) => {
          const linkUnflag = $(el);

          linkUnflag
            .on('click', (e) => {
              bag.trigger('RefreshView');
            });
        });
    }
  };
})(jQuery);
