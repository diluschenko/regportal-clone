'use strict';

(($) => {
  Drupal.behaviors.favoritesView = {
    attach(context) {
      const favorites = $('.view-display-id-favorites', context);

      $('.flag-favorites a', favorites)
        .once('bag_unflag')
        .each((i, el) => {
          const linkUnflag = $(el);

          linkUnflag
            .on('click', (e) => {
              favorites.trigger('RefreshView');
            });
        });
    }
  };
})(jQuery);
