// eslint-disable-next-line max-statements
(function ($) {

  const $body = $('body'),
    $win = $(window);

  // Fix the header to the top of the page.
  Drupal.behaviors.regportalFixedHeader = {
    attach(context) {
      const $head = $('.site-header'),
        $backToTop = $('.back-to-top'),
        $bodyHeight = $body.height(),
        $winHeight = $win.height(),
        $stickyXls = $('.download-favorites');
      let headHeight,
        offsetTop;

      function toggleFix(status) {
        const fixedClass = 'fixed';
        const showBackToTop = 'back-to-top_show';

        $body.toggleClass(fixedClass, status);
        $backToTop.toggleClass(showBackToTop, status);
      }

      function checkScrollOffset() {
        const scroll = $win.scrollTop();

        if ($bodyHeight > ($winHeight + 300)) {
          toggleFix(scroll >= offsetTop);
        }
      }

      function calcOffsetTop() {
        headHeight = $head.height();
        offsetTop = $head.offset().top + headHeight;
        $stickyXls.css('top', headHeight);
      }

      $('.site-header', context).once('fixed-header')
        .each(() => {
        calcOffsetTop();
      checkScrollOffset();

      if (offsetTop > 0) {
          $win.on({
            scroll: checkScrollOffset,
            resize() {
              calcOffsetTop();
              checkScrollOffset();
            },
          });
      }

      // no message no biggie
      else {
        toggleFix(true);
      }
    });

      $('.regportal-contacts-hide', context).on('click', function () {
        $body.toggleClass('show-contacts');
      });

      $('.regportal-contacts-show', context).on('click', function () {
        $body.toggleClass('show-contacts');
      });
    },
  };

  // Multiple select.
  Drupal.behaviors.adamosSelect = {
    attach: (context) => {
      $('select[multiple="multiple"]', context).once('multiple-select')
        .each((i, el) => {
          const $el = $(el);

          $el.fastselect({
            placeholder: $el.attr('data-placeholder'),
            noResultsText: Drupal.t('Не найдено'),
          });
        });
    },
  };


  // Auto resizing for textarea.
  Drupal.behaviors.adamosAutoResizeTextarea = {
    attach: (context) => {
      $('.form-textarea', context).once('resize-textarea')
        .each((i, el) => {
          const $el = $(el);

          $el.on('keyup.resize-textarea', () => {
            $el.css('height', 'auto');
            $el.height(el.scrollHeight);
          });
        });
    },
  };

  // Check if input field is filled.
  Drupal.behaviors.scrollToTop = {
    attach: (context) => {
      $('.back-to-top', context).once('scroll-to-top')
        .each((i, el) => {
          const $button = $(el);

          $button
            .on('click', (e) => {
              e.preventDefault();
              $('html, body', context).animate({scrollTop: 0}, 1000);
            });
        });
    },
  };

  // Disable bank account field when chosen no account.
  Drupal.behaviors.accountFilter = {
    attach: (context) => {
      $('.view-companies-filter', context).once('list-of-companies')
        .each((i, el) => {
          const $el = $(el),
          $accountSelect = $el.find('select[name="field_bank_target_id[]"]'),
          $par = $el.find('.form-item-field-bank-target-id');

          function statusAccountFilter(val) {
            if (val === '2') {
              $accountSelect.prop("disabled", false);
              $par.removeClass('disabled');
            }
            else {
              $accountSelect.prop("disabled", true);
              $par.addClass('disabled');
            }
            $accountSelect.trigger("chosen:updated");
          }

          $el.find('input[name="bank_account_filter"]')
            .once('form-select-change')
            .each((ind, elem) => {
              const radio = $(elem);
              radio
                .on('click', () => {
                  statusAccountFilter(radio.val());
              });
            });

          if ($el.find('input[name="bank_account_filter"]').length) {
            statusAccountFilter($el.find('input[name="bank_account_filter"]:checked').val());
          }
        });
    },
  };

  // Disable.
  Drupal.behaviors.datepickerForDates = {
    attach: (context) => {
      $('input[name="field_company_date_value"], input[name="field_company_date_value_1"]', context).datepicker({
        dateFormat: "dd.mm.yy",
      });
    },
  };

  // Close dialog by overlay.
  Drupal.behaviors.closeDialog = {
    attach: () => {
      $(document).on('click', '.ui-widget-overlay', (e) => {
        e.preventDefault();
        $('.ui-dialog-titlebar-close').click();
      });
    },
  };

  // Order button.
  Drupal.behaviors.orderButton = {
    attach: (context) => {
      $('.order-company', context)
        .once('colorbox')
        .each((ind, elem) => {
          const button = $(elem),
            parent = $(elem).closest('.node--type-company'),
            groupTitle = $(parent).find('.group-title'),
            groupTitleBuy = $(parent).find('.group-title-buy'),
            buttonBack = $(parent).find('.back-to-company-details'),
            webform = $(parent).find('.field--name-dynamic-block-fieldnode-buy-company'),
            desc = $(parent).find('.group-desc'),
            footer = $(parent).find('.company-footer');

          let counter = 0;

          function changeCompanyView() {
            $(webform).toggleClass('show');
            $(desc).toggleClass('hide');
            $(groupTitle).toggleClass('hide');
            $(groupTitleBuy).toggleClass('hide');
            $(footer).toggleClass('hide');

            if ($(webform).hasClass('show')) {

              if (counter === 0) {
                /* global yaCounter35057690 */
                /* eslint no-undef: "error" */
                yaCounter35057690.reachGoal('buy_firm');
              }
              counter += 1;
            }
          }


          button.on('click', (e) => {
            e.preventDefault();
            changeCompanyView();
          });

          buttonBack.on('click', (e) => {
            e.preventDefault();
            changeCompanyView();
          });

        });
    },
  };

  // Start search link.
  Drupal.behaviors.startSearchLink = {
    attach: (context) => {
      $('.start-search', context)
        .once('start-search')
        .each((ind, elem) => {
          const $link = $(elem),
            $search = $('.paragraph--type--companies');

          $link
            .on('click', (e) => {
              e.preventDefault();

              $('html, body').animate({scrollTop: $search.offset().top}, 500);
            });
        });
    },
  };


  // Start search link.
  Drupal.behaviors.scrollToElement = {
    attach: (context) => {
      $('.follow-link', context)
        .once('follow-link')
        .each((ind, elem) => {
          const $link = $(elem),
          $elem = $('.' + $link.data('follow'));

          $link
            .on('click', (e) => {
              if ($elem.length) {
                e.preventDefault();
                $('html, body').animate({scrollTop: $elem.offset().top}, 500);
              }
        });
      });
    },
  };

  Drupal.behaviors.regportalBarRating = {
    attach: (context) => {
    $('.field--name-field-comment-rating', context).once()
      .each((ind, elem) => {
        const $elem = $(elem),
          $rating = $elem.find('select');

        $rating.barrating({theme: 'css-stars'});
      });
    },
  };

  Drupal.behaviors.regportalAutocompleteComma = {
    attach: (context) => {
      $('input[name=field_company_okved_target_id]', context).once()
        .each((ind, elem) => {
          const $elem = $(elem);

          $elem.on("autocompleteselect", function(event, ui) {
            ui.item.value += ',';
          });
        });
    },
  };

  // Reset a bag view.
  Drupal.behaviors.regportalBagReload = {
    attach: (context) => {
      $('.regportal-user-bag', context)
        .once('reload-bag')
        .each((ind, elem) => {
          const $elem = $(elem),
            sum = $elem.find('.regportal-user-bag-price span'),
            counts = $elem.find('.regportal-user-bag-counts span');

          $elem.on('bag:reload', () => {
            $.ajax({
              type: 'GET',
              url: '/rest/get-user-bag-info?_format=json',
              success: (data) => {
                sum.text(data.sum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
                counts.text(data.counts);
              },
            });
          });

          $elem.trigger('bag:reload');
        });


      function action() {
        if (!$('.ui-dialog .flag-bag a').length) {
          setTimeout(action, 100);
        }
        $('.ui-dialog .flag-bag a')
          .once('reload-bag')
          .each((ind, elem) => {
            const $elem = $(elem);

            $elem
              .on('click', () => {
                $('.view-display-id-bag').trigger('RefreshView');
                setTimeout(() => {
                  $('.regportal-user-bag').trigger('bag:reload')
                }, 2000);
              });
          })
      }

      action();
    },
  };

  // Reset a bag view.
  Drupal.behaviors.regportalFavoritesCount = {
    attach: (context) => {
      const userFavoritesCount = $('.user-count-favorites', context);

      $('.flag-favorites a')
      .once('flag-favorites')
      .each((ind, elem) => {
        const $elem = $(elem);

        $elem
          .on('click', () => {
            if ($elem.parent().hasClass('action-flag')) {
              userFavoritesCount.text(parseInt(userFavoritesCount.text().replace(/[^0-9]/g, ''), 10) + 1);
            }
            else {
              userFavoritesCount.text(parseInt(userFavoritesCount.text().replace(/[^0-9]/g, ''), 10) - 1);
            }
          });
      })
    },
  };

  // Reset a bag view.
  Drupal.behaviors.regportalLeftSidebarMenu = {
    attach: (context) => {
      $('.site-main--left', context)
        .once('side-bar-left')
        .each((ind, elem) => {
          const $elem = $(elem),
          $btn = $elem.find('.nav-toggle');

          $btn
            .on('click', (e) => {
              e.preventDefault();
              $elem.toggleClass('open');
            });
        });
    },
  };

  // Reset a bag view.
  Drupal.behaviors.regportalMainMenu = {
    attach: (context) => {
      $('.site-header', context)
        .once('site-header-menu')
        .each((ind, elem) => {
          const $parent = $(elem).find('.layout-container'),
            $btn = $parent.find('.nav-toggle');

          $btn
            .on('click', (e) => {
              e.preventDefault();
              $parent.toggleClass('open');
            });
        });
    },
  };

  // Order button.
  Drupal.behaviors.yaCounters = {
    attach: (context) => {
      $('.download-favorites', context)
        .on('click', () => {
          yaCounter35057690.reachGoal('upload_favorites');
        });

      $('.view-filters-additional-info .xlsx-feed a', context)
        .on('click', () => {
          yaCounter35057690.reachGoal('upload_detected');
        });
    },
  }

  // Views filter's header.
  Drupal.behaviors.regportalViewFilter = {
    attach: (context) => {
      $('.view-companies-filter', context)
        .once('view-company-filter')
        .each((ind, elem) => {
          const $view = $(elem),
            $filter = $view.find('.view-filters > form'),
            $filterBtn = $view.find('.filter-btn');

          if (!$filterBtn.hasClass('on')) {
            $filter.hide();
          }

          function filterHandler() {
            if ($filterBtn.hasClass('on')) {
              $filterBtn.removeClass('on');
              $filterBtn.html(Drupal.t('Развернуть'));
              $filter.slideUp();
            }
            else {
              $filterBtn.addClass('on');
              $filterBtn.html(Drupal.t('Свернуть'));
              $filter.slideDown();
            }
          }

          $filterBtn
            .once('filter')
            .on('click', (e) => {
              e.preventDefault();
              filterHandler();
            });
        });
    },
  };

}(jQuery));
