'use strict';

(($) => {
  Drupal.behaviors.discountsView = {
    attach(context) {
      const discounts = $('.view-display-id-block_order_discount', context);

      let totalDiscount = 0;

      $('tbody .views-field-field-order-discount', discounts)
        .once('discounts')
        .each((i, el) => {
          const discount = $(el);

          totalDiscount += parseInt(discount.text(), 10);
        });

      $('.total-discount span', discounts).text(totalDiscount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
    }
  };
})(jQuery);
