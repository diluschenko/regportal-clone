'use strict';

(($) => {
  Drupal.behaviors.Orders = {
    attach(context) {
      const view = $('.view-display-id-orders', context);

      $('.order-main-info', view)
        .once('order-main-info')
        .each((i, el) => {
          const row = $(el);

          row
            .on('click', (e) => {
              row.toggleClass('open');
            });
        });
    }
  };
})(jQuery);
