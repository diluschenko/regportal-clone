'use strict';


// require modules
const fs = require('fs');
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const buffer = require('vinyl-buffer');
const plumber = require('gulp-plumber');
const merge = require('merge-stream');
// file-related
const runSequence = require('run-sequence');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');
const rename = require('gulp-rename');
const changed = require('gulp-changed');
// (s)css
const sass = require('gulp-sass');
const sassGlob = require('gulp-sass-glob');
const sassLint = require('gulp-sass-lint');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const mqpacker = require('css-mqpacker');
// js
const modernizr = require('gulp-modernizr');
const eslint = require('gulp-eslint');
const babel = require('gulp-babel');
// img
const imagemin = require('gulp-imagemin');
const compassImagehelper = require('gulp-compass-imagehelper');
const spritesmith = require('gulp.spritesmith-multi');


// load config from file
const config = JSON.parse(fs.readFileSync('./gulp-config.json'));


// watch-task default output
function watchOutput(event) {
  console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
}


// # resources
// images
gulp.task('img:min', () => {
  var source = [].concat(config.img.src);

  // prevent sprited images to be copied
  config.img.sprites.files.forEach((path) => {
    var files = config.img.srcPath + path;
    source.push('!' + files.split('*')[0]);
    source.push('!' + files);
  });

  // minify the images
  return gulp.src(source)
    .pipe(changed(config.img.dest))
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}]
    }))
    .pipe(gulp.dest(config.img.dest))
    .pipe(browserSync.stream());
});

// Generating img:maps for usage in sass
gulp.task('img:map', () => {
  return gulp.src(config.img.dest + '/**/*.+(jpeg|jpg|png|gif|svg)')
    .pipe(compassImagehelper({
      images_path: config.img.dest,
      css_path:    config.css.dest,
      template:    'image-map.handlebars',
      targetFile:  'variables/_image-map.scss',
      prefix:      'images-'
    }))
    .pipe(gulp.dest(config.css.path));
});

// generate sprites
gulp.task('img:sprite', () => {
  var spriteFolders = [];

  config.img.sprites.files.forEach((path) => {
    spriteFolders.push(config.img.srcPath + path);
  });

  // Generate our spritesheet
  var spriteData = gulp.src(spriteFolders)
    .pipe(spritesmith({
      spritesmith: function (options) {
        options.imgPath = options.imgName;
        options.cssName = '_sprites/_sprite-' + options.imgName.split('.')[0] + '.scss';
        options.cssTemplate = 'image-sprite.handlebars'
      }
    }));

  // Pipe image stream through image optimizer and onto disk
  var imgStream = spriteData.img.pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest(config.img.dest));

  // Pipe CSS stream through CSS optimizer and onto disk
  var cssStream = spriteData.css.pipe(buffer())
    .pipe(imagemin())
    .pipe(gulp.dest(config.css.path + '/variables'));

  // Return a merged stream to handle both `end` events
  return merge(imgStream, cssStream);
});

//
gulp.task('img:clean', () => {
  return gulp.src([
    config.img.dest,
    config.css.path + '/variables/_sprites'
  ], {read: false})
    .pipe(clean());
});

// combined image-task
gulp.task('img', (callback) => {
  runSequence(['img:sprite', 'img:min'], 'img:map', callback);
});

// image watch task
gulp.task('watch:img', () => {
  return gulp
    .watch(config.img.src, ['image'])
    .on('change', watchOutput);
});


// copy lib-files (non js/css) from source
gulp.task('lib:copy', () => {
  let source = [];

  // different destination
  config.lib.src.forEach((item, i) => {
    if (typeof item === 'object') {
      gulp.src(item[0])
        .pipe(gulp.dest(config.lib.dest + '/' + item[1]));
    }
    else {
      source.push(item);
    }
  });

  // default destination
  return gulp.src(source)
    .pipe(gulp.dest(config.lib.dest));
});

// clean lib files
gulp.task('lib:clean', () => {
  return gulp.src([config.lib.dest], {read: false})
    .pipe(clean());
});

//
gulp.task('lib:modernizr', () => {
  // drupal 8 defaults
  return gulp.src(config.js.src.concat(config.css.src))
    .pipe(modernizr(
      config.modernizr.files.custom,
      {
        options: config.modernizr.options,
        tests:   config.modernizr.tests
      }
    ))
    .pipe(gulp.dest(config.lib.dest));
});

// combines lib task
gulp.task('lib', (callback) => {
  runSequence('lib:clean', 'lib:modernizr', 'lib:copy', callback);
});


// # sass (css)
// clean complied css files
gulp.task('css:clean', () => {
  return gulp.src([config.css.dest], {read: false})
    .pipe(clean());
});

// Linting the styles.
gulp.task('sass:lint', () => {
  // .sass-lint.yml is used for config
  return gulp.src([config.css.src])
    .pipe(sassLint({
      options:    {
        formatter: 'stylish'
      },
      files:      {
        ignore: config.css.lintIgnore
      },
      configFile: '.sass-lint.yml'
    }))
    .pipe(sassLint.format())
    .pipe(sassLint.failOnError())
});

// Concat css from vendor content
gulp.task('css:vendor', () => {
  return gulp.src(config.css.vendor)
    .pipe(sourcemaps.init())
    .pipe(concat(config.css.files.vendor))
    .pipe(sourcemaps.write())
    .pipe(rename({
      prefix: config.theme + config.fileConcat
    }))
    .pipe(gulp.dest(config.css.dest))
    .pipe(browserSync.stream());
});

// Compile css from source
gulp.task('css:custom', () => {
  return gulp.src(config.css.src)
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sassGlob())
    .pipe(sass({
      outputStyle:     config.css.style,
      includePaths:    config.css.includes,
      errLogToConsole: true
    })
      .on('error', sass.logError))
    .pipe(postcss([
      autoprefixer({browsers: config.css.autoprefix}),
      mqpacker({sort: true})
    ]))
    .pipe(rename({prefix: config.theme + config.fileConcat}))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.css.dest))
    .pipe(browserSync.stream());
});

// combined css-generation task
gulp.task('css', ['css:vendor', 'css:custom']);

// css watch-task
gulp.task('watch:css', () => {
  return gulp
    .watch(config.css.src, ['css:custom'])
    .on('change', watchOutput);
});


// # js
// clean complied css files
gulp.task('js:clean', () => {
  return gulp.src([config.js.dest], {read: false})
    .pipe(clean());
});

// De-lint the custom js files
gulp.task('js:lint', () => {
  return gulp.src(config.js.lint)
    .pipe(eslint({
      globals: config.js.globals
    }))
    .pipe(eslint.format('stylish'));
});

// concat vendor js
gulp.task('js:vendor', () => {
  return gulp.src(config.js.vendor)
    .pipe(sourcemaps.init())
    .pipe(concat(config.js.files.vendor))
    .pipe(sourcemaps.write())
    .pipe(rename({prefix: config.theme + config.fileConcat}))
    .pipe(gulp.dest(config.js.dest));
});

// Use next generation JavaScript, today.
gulp.task('js:es6', () => {
  return gulp.src(config.js.dest + '/*.js')
    .pipe(babel({
      presets: config.js.presents
    }))
    .pipe(gulp.dest(config.js.dest))
});

// concat custom js
gulp.task('js:custom', ['js:lint'], () => {
  if (config.js.concat) {
    // concat
    return gulp.src(config.js.src)
      .pipe(plumber())
      .pipe(sourcemaps.init())
      .pipe(concat(config.js.files.custom))
      .pipe(sourcemaps.write())
      .pipe(rename({prefix: config.theme + config.fileConcat}))
      .pipe(gulp.dest(config.js.dest))
      .pipe(browserSync.stream());
  }
  else {
    // prefixing only
    return gulp.src(config.js.src)
      .pipe(rename({
        prefix: config.theme + config.fileConcat
      }))
      .pipe(gulp.dest(config.js.dest))
      .pipe(browserSync.stream());
  }
});

// combined tasks
gulp.task('js', ['js:vendor', 'js:custom', 'js:es6']);

// js watch-task
gulp.task('watch:js', () => {
  return gulp
    .watch(config.js.src, ['js:custom', 'lib:modernizr'])
    .on('change', watchOutput);
});


// Static Server + watching scss files
gulp.task('serve', ['dev:build'], () => {
  // use proxy to work in a theme
  browserSync.init({
    proxy:  config.proxy,
    open:   false,
    notify: false
  });

  gulp.watch(config.css.src, ['css:custom']);
  gulp.watch(config.js.src, ['js:custom', 'lib:modernizr']);
  gulp.watch(config.img.src, ['img', 'css:custom']);

  gulp.watch([
    config.js.src + '*.js',
    config.img.src,
    '*.tpl.php'])
    .on('change', browserSync.reload);
});


// dev
gulp.task('dev:clean', ['lib:clean', 'css:clean', 'js:clean']);

gulp.task('dev:build', (callback) => {
  runSequence('dev:clean', ['img', 'lib'], ['js', 'css'], callback);
});


// Build / Distribution
gulp.task('build:clean', ['img:clean', 'lib:clean', 'css:clean', 'js:clean']);

gulp.task('build', (callback) => {
  runSequence('build:clean', ['img', 'lib'], ['js', 'css'], callback);
});

// remove all development files
gulp.task('dist:clean', () => {
  return gulp.src(
    [
      config.src,
      './node_modules',
      '.bowerrc',
      '.eslintrc.json',
      '.sass-lint.yml',
      'bower.json',
      'gulp-config.json',
      'gulpfile.js',
      'image-map.handlebars',
      'image-sprite.handlebars',
      'npm-post.sh',
      'package.json',
      'readme.md'
    ].concat(config.clean),
    {read: false}
  )
    .pipe(clean());
});

// build and clean
gulp.task('dist', (callback) => {
  runSequence('build', 'dist:clean', callback);
});


// General / default
gulp.task('test', ['js:lint', 'sass:lint']);
gulp.task('watch', ['build', 'watch:js', 'watch:img', 'watch:css']);
gulp.task('default', ['serve']);
